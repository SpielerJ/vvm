/** @type {import('tailwindcss').Config} config */
import forms from '@tailwindcss/forms';
import tailwindcssRfs from 'tailwindcss-rfs';

const config = {
  content: ['./index.php', './app/**/*.php', './resources/**/*.{php,vue,js}'],
  theme: {
    colors: {
      'transparent' : 'transparent',
      'teal' : '#014953',
      'yellow': {
        light: '#FEFFEE',
        DEFAULT: '#FAFFBC',
      },
      'white' : '#FFF',
      'white/50' : 'rgba(255,255,255,.5)',
      'black' : '#000',
      'blue' : '#235AE6',
      'red' : '#f50f0f',
      'gray' : {
        light: '#F1F1F1',
        DEFAULT: '#717171',
      },
    }, // Extend Tailwind's default colors
    extend: {
      screens: {
        'sm': '600px',
        'md': '780px',
        'lg': '1024px',
        'xl': '1320px',
      },
      fontFamily: {
        'sans': ['"Satoshi"', 'system-ui'],
        'display': ['"Big Shoulders Display"']
      },
      fontSize: {
        sm: '0.8rem',
        base: '1rem',
        lg: 'clamp(1rem, calc(16vw / 15.3489), 1.125rem)', //min 16px, 18px
        xl: 'clamp(1.125rem, calc(20vw / 15.3489), 1.25rem)',  //min 18px, 20px
        '2xl': 'clamp(1.25rem, calc(22vw / 15.3489), 1.375rem)',  //min 20px, 22px
        '3xl': 'clamp(1.5rem, calc(34vw / 15.3489), 2.125rem)', // min 24px, 34px //h4
        '4xl': 'clamp(1.875rem, calc(38vw / 15.3489), 2.375rem)', // min 30px, 38px //h3
        '5xl': 'clamp(1.875rem, calc(45vw / 15.3489), 2.8125rem)', // min 30px, 45px //h2
        '6xl': 'clamp(2.8125rem, calc(60vw / 15.3489), 3.75rem)', // min 45px, 60px
        '7xl': 'clamp(2.8125rem, calc(70vw / 15.3489), 4.375rem)', // min 45px, 70px  //h1
      },
      lineHeight: {
        'normal': '1.4',
      },
      textUnderlineOffset: {
        3: '3px',
      },
    },
  },
  safelist: [
    'underline',
    '!grid',
    {
      pattern: /stroke-(teal|yellow|white|black|blue|red)/
    },
    {
      pattern: /fill-(teal|yellow|white|black|blue|red)/
    },
    {
      pattern: /rounded-(t|r|b|l)-(sm|md|lg)/
    },
    {
      pattern: /grid-cols-(2|3|4|5|6)/,
      variants: ['lg', 'md', 'sm'],
    }
  ],
  plugins: [
    forms,
    tailwindcssRfs
  ],
};

export default config;