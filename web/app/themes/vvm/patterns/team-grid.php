<?php
/**
 * Title: Kachelansicht des Teams
 * Slug: vvm/team-grid
 * Categories: team
 * Description: Header für Produkte / Fokusthemen
 * Keywords: team, block, custom
 * Block Types: core/title, core/paragraph, acf/employeelist, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:heading {"textAlign":"center","textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color">Das VVM-Team</h2>
<!-- /wp:heading -->

<!-- wp:acf/employeelist {"name":"acf/employeelist","data":{"profession-terms":"","_profession-terms":"field_employeelist_profession-terms","employees":"","_employees":"field_employeelist_employees"},"align":"","mode":"preview","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}}} /-->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"textAlign":"left"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-text-align-left wp-element-button">Teil des Teams werden</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->