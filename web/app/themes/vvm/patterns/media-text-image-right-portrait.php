<?php
/**
 * Title: Text + Bild rechts (vertikal)
 * Slug: vvm/media-text-image-right-portrait
 * Categories: media, text
 * Description: Text + Bild Sektion
 * Keywords: media, image, block, custom
 * Block Types: core/media-text
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","backgroundColor":"gray-light","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-gray-light-background-color has-background"><!-- wp:media-text {"align":"","mediaPosition":"right","mediaId":425,"mediaLink":"http://vvm.meta-maniacs.de/ueber-uns/gruppe-maskieren-62x/","mediaType":"image","mediaWidth":40,"verticalAlignment":"top","imageFill":false,"style":{"spacing":{"padding":{"top":"0","bottom":"0","left":"0","right":"0"}}}} -->
<div class="wp-block-media-text has-media-on-the-right is-stacked-on-mobile is-vertically-aligned-top" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;grid-template-columns:auto 40%"><div class="wp-block-media-text__content"><!-- wp:heading {"style":{"spacing":{"margin":{"top":"0"}}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color" style="margin-top:0">LOREM IPSUM DOLOR SIT AMET DEUS QUANTIMUS EOS</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"spacing":{"margin":{"right":"0","bottom":"0"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-right:0;margin-bottom:0">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
<!-- /wp:paragraph --></div><figure class="wp-block-media-text__media"><img src="http://vvm.meta-maniacs.de/app/uploads/2023/09/Gruppe-maskieren-6@2x-751x1024.png" alt="" class="wp-image-425 size-full"/></figure></div>
<!-- /wp:media-text --></div>
<!-- /wp:group -->