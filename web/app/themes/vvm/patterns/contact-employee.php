<?php
/**
 * Title: Mitarbeiter Kontakt
 * Slug: vvm/contact-employee
 * Categories: contact, team
 * Description: Mitarbeiter Kontakt
 * Keywords: contact, block, custom
 * Block Types: acf/employeecontact
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:acf/employeecontact {"name":"acf/employeecontact","data":{"employee":381,"_employee":"field_employeecontact_employee","heading":"","_heading":"field_employeecontact_heading"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->