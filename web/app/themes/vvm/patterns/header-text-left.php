<?php
/**
 * Title: Header - Text links
 * Slug: vvm/header-text-left
 * Categories: header, call-to-action
 * Description: Header mit Inhalt links
 * Keywords: header, pattern, block, custom
 * Block Types: core/cover, core/heading, core/paragraph, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:cover {"url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/pexels-john-diez-7578702-scaled.jpg","id":484,"dimRatio":50,"focalPoint":{"x":0.5,"y":0.93},"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull"><span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span><img class="wp-block-cover__image-background wp-image-484" alt="" src="http://vvm.meta-maniacs.de/app/uploads/2023/10/pexels-john-diez-7578702-scaled.jpg" style="object-position:50% 93%" data-object-fit="cover" data-object-position="50% 93%"/><div class="wp-block-cover__inner-container"><!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"36%"} -->
<div class="wp-block-column" style="flex-basis:36%"><!-- wp:heading {"textAlign":"left","level":1,"textColor":"yellow-default"} -->
<h1 class="wp-block-heading has-text-align-left has-yellow-default-color has-text-color">Cyber-<br>versicherung</h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"yellow-default","fontSize":"xl"} -->
<p class="has-yellow-default-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--50)">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Mehr erfahren</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div></div>
<!-- /wp:cover -->