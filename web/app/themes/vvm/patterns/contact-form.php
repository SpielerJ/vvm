<?php
/**
 * Title: Kontakt
 * Slug: vvm/contact-form
 * Categories: contact
 * Description: Kontaktformular
 * Keywords: contact, form, block, custom
 * Block Types: core/paragraph, core/heading, contact-form-7/contact-form-selector,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|80"}}}} -->
<div class="wp-block-columns"><!-- wp:column {"width":"35%"} -->
<div class="wp-block-column" style="flex-basis:35%"><!-- wp:heading {"style":{"typography":{"textTransform":"uppercase"}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color" style="text-transform:uppercase">Kontaktieren Sie uns</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size">Sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0","bottom":"0"}}}} -->
<div class="wp-block-column" style="padding-top:0;padding-bottom:0"><!-- wp:contact-form-7/contact-form-selector {"id":175,"hash":"ac42cea","title":"Kontakformular"} -->
<div class="wp-block-contact-form-7-contact-form-selector">[contact-form-7 id="ac42cea" title="Kontakformular"]</div>
<!-- /wp:contact-form-7/contact-form-selector --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->