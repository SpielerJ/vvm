<?php
/**
 * Title: CTA mit Hintergrundbild
 * Slug: vvm/media-cta
 * Categories: media, call-to-action
 * Description: großes CTA mit Hintergrundbild
 * Keywords: cta, heading, block, custom
 * Block Types: core/cover, core/heading, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:cover {"url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/Gruppenbild-SZ-SB-VV-DH.jpg","id":731,"dimRatio":20,"focalPoint":{"x":0.48,"y":0.29},"minHeight":350,"isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|80","bottom":"var:preset|spacing|80"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull is-light" style="padding-top:var(--wp--preset--spacing--80);padding-bottom:var(--wp--preset--spacing--80);min-height:350px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-731" alt="" src="http://vvm.meta-maniacs.de/app/uploads/2023/10/Gruppenbild-SZ-SB-VV-DH.jpg" style="object-position:48% 29%" data-object-fit="cover" data-object-position="48% 29%"/><div class="wp-block-cover__inner-container"><!-- wp:heading {"textAlign":"center","style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"bottom":"var:preset|spacing|50","top":"var:preset|spacing|70"}}},"textColor":"yellow-default","fontSize":"7xl"} -->
<h2 class="wp-block-heading has-text-align-center has-yellow-default-color has-text-color has-7-xl-font-size" style="margin-top:var(--wp--preset--spacing--70);margin-bottom:var(--wp--preset--spacing--50);text-transform:uppercase">Wir sind VVM</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"},"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|70"}}}} -->
<div class="wp-block-buttons" style="margin-bottom:var(--wp--preset--spacing--70)"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Mehr über uns erfahren</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div></div>
<!-- /wp:cover -->