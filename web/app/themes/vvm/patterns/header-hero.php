<?php
/**
 * Title: Seiten Hero
 * Slug: vvm/header-hero
 * Categories: header
 * Description: Header mit Inhalt links
 * Keywords: header, hero, block, custom
 * Block Types: acf/hero, core/heading, core/paragraph, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:acf/hero {"name":"acf/hero","data":{"hero_image":142,"_hero_image":"field_hero_hero_image","slogan":"  Versichern. Vertrauen. Machen.","_slogan":"field_hero_slogan"},"align":"full","mode":"preview"} -->
<!-- wp:group {"style":{"spacing":{"padding":{"right":"0","left":"0","top":"var:preset|spacing|70","bottom":"var:preset|spacing|80"}}},"layout":{"type":"flex","orientation":"vertical","justifyContent":"left","verticalAlignment":"center"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--70);padding-right:0;padding-bottom:var(--wp--preset--spacing--80);padding-left:0"><!-- wp:heading {"level":1,"placeholder":"Deine Überschrift","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"}}},"textColor":"teal"} -->
<h1 class="wp-block-heading has-teal-color has-text-color" style="margin-bottom:var(--wp--preset--spacing--30)">Große Verantwortung in guten Händen</h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"Deine Text","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--50)">Wichtige Entscheidungen brauchen Vertrauen. Und die Absicherung durch einen erfahrenen Partner, der im Geschäftsalltag den Rücken frei hält.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Kontakt aufnehmen</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->
<!-- /wp:acf/hero -->