<?php
/**
 * Title: FAQ-Akkordeon
 * Slug: vvm/text-faq
 * Categories: text
 * Description: Akkordeon mit Fragen und Antworten
 * Keywords: faq, akkordeon, block, custom
 * Block Types: core/heading, core/details,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","backgroundColor":"transparent","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-transparent-background-color has-background"><!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|60"}}}} -->
<h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--60)">Häufig gefragt</h2>
<!-- /wp:heading -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|teal","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--teal);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|teal","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--teal);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|teal","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--teal);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|teal","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--teal);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details --></div>
<!-- /wp:group -->