<?php
/**
 * Title: Intro Text
 * Slug: vvm/text-intro
 * Categories: text
 * Description: großer Introtext
 * Keywords: intro, text, block, custom
 * Block Types: core/paragraph,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0","padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"backgroundColor":"teal","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-teal-background-color has-background" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)"><!-- wp:paragraph {"style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"top":"var:preset|spacing|40","bottom":"0"}}},"textColor":"white","fontSize":"base"} -->
<p class="has-white-color has-text-color has-base-font-size" style="margin-top:var(--wp--preset--spacing--40);margin-bottom:0;text-transform:uppercase">Lorem Ipsum</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"lineHeight":"1.4"},"spacing":{"margin":{"bottom":"var:preset|spacing|40"}}},"textColor":"white","fontSize":"4xl"} -->
<p class="has-white-color has-text-color has-4-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--40);line-height:1.4">VVM ist das Maklerbüro für Menschen in Verantwortung. Mit klarem Fokus auf umfassende geschäftliche Absicherung. Wir schaffen Transparenz auf einem unübersichtlichen Markt und sind die verlässliche Begleitung durch alle Höhen und Tiefen des unternehmerischen Wegs.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->