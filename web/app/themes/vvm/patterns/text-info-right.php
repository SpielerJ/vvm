<?php
/**
 * Title: Text + Infobox rechts
 * Slug: vvm/text-info-right
 * Categories: text
 * Description: Text mit Infokasten rechts
 * Keywords: info, text, block, custom
 * Block Types: core/heading, core/paragraph, acf/icon, core/list
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","backgroundColor":"gray-light","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-gray-light-background-color has-background"><!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|70"}}}} -->
<div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"top","width":"50%"} -->
<div class="wp-block-column is-vertically-aligned-top" style="flex-basis:50%"><!-- wp:heading {"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color">Lorem ipsum dolor sit amet deus quantimus eos</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|40"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--40)">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50","left":"var:preset|spacing|50","right":"var:preset|spacing|50"}}},"backgroundColor":"teal","textColor":"white","layout":{"type":"flex","orientation":"vertical","justifyContent":"left","verticalAlignment":"top","flexWrap":"wrap"}} -->
<div class="wp-block-group has-white-color has-teal-background-color has-text-color has-background" style="padding-top:var(--wp--preset--spacing--50);padding-right:var(--wp--preset--spacing--50);padding-bottom:var(--wp--preset--spacing--50);padding-left:var(--wp--preset--spacing--50)"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0","bottom":"0"},"margin":{"top":"0","bottom":"var:preset|spacing|50"}}},"backgroundColor":"transparent","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group has-transparent-background-color has-background" style="margin-top:0;margin-bottom:var(--wp--preset--spacing--50);padding-top:0;padding-bottom:0"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"alert-circle","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"0","_auto-width":"field_icon_auto-width","width":"32","_width":"field_icon_width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview","style":{"spacing":{"margin":{"right":"var:preset|spacing|20"}}}} /-->

<!-- wp:heading {"level":4,"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}},"textColor":"white"} -->
<h4 class="wp-block-heading has-white-color has-text-color" style="margin-top:0;margin-bottom:0">LÖSUNG DURCH VVM</h4>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:list {"type":"1","style":{"spacing":{"margin":{"left":"var:preset|spacing|30"}}}} -->
<ul type="1" class="wp-block-list"><!-- wp:list-item {"fontSize":"lg"} -->
<li class="has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat</li>
<!-- /wp:list-item -->

<!-- wp:list-item {"fontSize":"lg"} -->
<li class="has-lg-font-size">Sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum</li>
<!-- /wp:list-item -->

<!-- wp:list-item {"fontSize":"lg"} -->
<li class="has-lg-font-size">Donsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->