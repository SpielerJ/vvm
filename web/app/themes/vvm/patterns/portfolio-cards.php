<?php
/**
 * Title: Cards Leistungsversprechen
 * Slug: vvm/portfolio-cards
 * Categories: portfolio
 * Description: drei Karten
 * Keywords: cards
 * Block Types: core/group, core/heading, core/paragraph, acf/icon, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"yellow-light","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull has-yellow-light-background-color has-background"><!-- wp:heading {"textAlign":"center","textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color">Unser Leistungsversprechen</h2>
<!-- /wp:heading -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"},"blockGap":{"top":"0","left":"0"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column -->

<!-- wp:column {"width":"80%"} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:paragraph {"align":"center","textColor":"teal","fontSize":"xl"} -->
<p class="has-text-align-center has-teal-color has-text-color has-xl-font-size">Verbindlichkeit sorgt für Verlässlichkeit. Unser Leistungsversprechen ist das verbindliche Angebot, das wir machen und unseren Kund:innen jeden Tag aufs Neue erfüllen.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"className":"cards"} -->
<div class="wp-block-columns cards" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"black","layout":{"type":"flex","orientation":"vertical","verticalAlignment":"top"}} -->
<div class="wp-block-group has-black-background-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"fontStyle":"normal","fontWeight":"900","textTransform":"none"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"textColor":"white","fontSize":"lg"} -->
<p class="has-white-color has-text-color has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"pinkie-finger","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview","style":{"layout":{"selfStretch":"fit","flexSize":null}}} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"teal","textColor":"white","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-white-color has-teal-background-color has-text-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"fontStyle":"normal","fontWeight":"900","textTransform":"none"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"fontSize":"lg"} -->
<p class="has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr,</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"reward-stars-2","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"blue","layout":{"type":"flex","orientation":"vertical","justifyContent":"left"}} -->
<div class="wp-block-group has-blue-background-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"textTransform":"none","fontStyle":"normal","fontWeight":"900"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"textColor":"white","fontSize":"lg"} -->
<p class="has-white-color has-text-color has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr.</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"heart-approve-2","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview","style":{"spacing":{"margin":{"right":"var:preset|spacing|20","left":"var:preset|spacing|20"}}}} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"},"blockGap":{"top":"0","left":"0"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column -->

<!-- wp:column {"width":"80%"} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:paragraph {"align":"center","textColor":"teal","fontSize":"xl"} -->
<p class="has-text-align-center has-teal-color has-text-color has-xl-font-size">Transparenz im Handeln und eine offene, verbindliche Kommunikation sind die Grundlage einer gesunden Zusammenarbeit. Wie dieser Ansatz zu spürbarem Mehrwert für unsere Kund:innen wird, haben wir hier gesondert umrissen:</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"yellow","textColor":"teal"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-teal-color has-yellow-background-color has-text-color has-background wp-element-button">Mehr dazu</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></section>
<!-- /wp:group -->