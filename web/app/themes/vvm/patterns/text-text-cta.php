<?php
/**
 * Title: Text + CTA
 * Slug: vvm/text-text-cta
 * Categories: text, call-to-action
 * Description: Text mit Überschrift und CTA
 * Keywords: cta, text, block, custom
 * Block Types: core/heading, core/paragraph, core/buttons
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"80%"} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:heading {"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color">Lorem ipsum dolar Sit atmet</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--50)">Sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum sol ardesse estasi cum ocilum eros. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie cumque consequat. Vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla lorem ipsum eosque facilisi.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"left"}} -->
<div class="wp-block-buttons"><!-- wp:button {"textAlign":"left"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-text-align-left wp-element-button">Mehr erfahren</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->