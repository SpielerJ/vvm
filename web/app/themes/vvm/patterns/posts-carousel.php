<?php
/**
 * Title: Beitragskarussel
 * Slug: vvm/posts-carousel
 * Categories: posts
 * Description: Ein Karussel mit beliebigen Beiträgen
 * Keywords: posts, carousel, block, custom
 * Block Types: core/paragraph, core/heading, acf/postcarousel,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"bottom":"var:preset|spacing|80"}}},"backgroundColor":"white","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="padding-bottom:var(--wp--preset--spacing--80)"><!-- wp:heading {"textAlign":"center","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color" style="margin-bottom:var(--wp--preset--spacing--50)">Fokusthemen</h2>
<!-- /wp:heading -->

<!-- wp:acf/postcarousel {"name":"acf/postcarousel","data":{"field_postcarousel_post_type":"focustopic","field_postcarousel_num-posts":"6 Beiträge"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->