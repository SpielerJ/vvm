<?php
/**
 * Title: Text + Bild links (horizontal)
 * Slug: vvm/media-text-image-left-landscape
 * Categories: media, text
 * Description: Text + Bild Sektion
 * Keywords: media, image, block, custom
 * Block Types: core/media-text
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","backgroundColor":"gray-light","className":"image-offset","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull image-offset has-gray-light-background-color has-background"><!-- wp:media-text {"align":"","mediaId":733,"mediaLink":"http://vvm.meta-maniacs.de/ueber-uns/gruppenbild-vs-sb-sr/","mediaType":"image","mediaWidth":40,"verticalAlignment":"top","imageFill":false,"style":{"spacing":{"padding":{"top":"0","bottom":"0","left":"0","right":"0"}}},"className":"image-offset"} -->
<div class="wp-block-media-text is-stacked-on-mobile is-vertically-aligned-top image-offset" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;grid-template-columns:40% auto"><figure class="wp-block-media-text__media"><img src="http://vvm.meta-maniacs.de/app/uploads/2023/10/Gruppenbild-VS-SB-SR-1024x683.jpg" alt="" class="wp-image-733 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color">Lorem ipsum dolar sit atmet deus quantimus eos</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"0"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-bottom:0">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text --></div>
<!-- /wp:group -->