<?php
/**
 * Title: Zitat mit Hintergrundbild
 * Slug: vvm/text-quote
 * Categories: text, testimonials
 * Description: großes Zitat
 * Keywords: zitat, text, block, custom
 * Block Types: core/cover, core/quote,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:cover {"url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/pexels-john-diez-7578686_waermer-scaled.jpg","id":483,"dimRatio":50,"focalPoint":{"x":0.48,"y":0.66},"minHeight":500,"minHeightUnit":"px","align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull" style="min-height:500px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span><img class="wp-block-cover__image-background wp-image-483" alt="" src="http://vvm.meta-maniacs.de/app/uploads/2023/10/pexels-john-diez-7578686_waermer-scaled.jpg" style="object-position:48% 66%" data-object-fit="cover" data-object-position="48% 66%"/><div class="wp-block-cover__inner-container"><!-- wp:quote {"align":"center","textColor":"yellow-default"} -->
<blockquote class="wp-block-quote has-text-align-center has-yellow-default-color has-text-color"><!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|20"}}},"fontSize":"6xl"} -->
<p class="has-6-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--20)">33% der Unternehmen in Deutschland werden jährlich Ziel von Cyberkriminalität.</p>
<!-- /wp:paragraph --><cite>Quellenangabe</cite></blockquote>
<!-- /wp:quote --></div></div>
<!-- /wp:cover -->