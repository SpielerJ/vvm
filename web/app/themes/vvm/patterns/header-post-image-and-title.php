<?php
/**
 * Title: Header für Produkte / Fokusthemen
 * Slug: vvm/header-post_image_and_title
 * Categories: header
 * Description: Header für Produkte / Fokusthemen
 * Keywords: header, pattern, block, custom
 * Block Types: core/cover, core/title, core/paragraph
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:cover {"useFeaturedImage":true,"dimRatio":50,"focalPoint":{"x":0.5,"y":0.25},"isDark":false,"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull is-light"><span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span><div class="wp-block-cover__inner-container"><!-- wp:post-title {"textAlign":"center","level":1,"textColor":"yellow-default"} /-->

<!-- wp:columns {"style":{"spacing":{"blockGap":{"top":"0","left":"0"}}}} -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"></div>
<!-- /wp:column -->

<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%"><!-- wp:paragraph {"align":"center","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"yellow-default","fontSize":"xl"} -->
<p class="has-text-align-center has-yellow-default-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--50)">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam eos nonumy eirmod tempor invidunt ut labore et dolore magna sunt. Labore et dolore magna aliquyam erat, sed diam voluptua sol ordet.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Mehr erfahren</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div></div>
<!-- /wp:cover -->