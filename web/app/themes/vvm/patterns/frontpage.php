
<?php
/**
 * Title: Startseite
 * Slug: vvm/frontpage
 * Categories: theme
 * Description: Startseite
 * Keywords: frontpage, homepage, custom
 * Block Types: core/post-content
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:cover {"overlayColor":"teal","minHeight":100,"minHeightUnit":"vh","contentPosition":"center center","align":"full","className":"intro","layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull intro" style="min-height:100vh"><span aria-hidden="true" class="wp-block-cover__background has-teal-background-color has-background-dim-100 has-background-dim"></span><div class="wp-block-cover__inner-container"><!-- wp:safe-svg/svg-icon {"svgURL":"http://vvm.meta-maniacs.de/app/uploads/2023/10/intro-animation.svg","alignment":"center","imageID":835,"imageWidth":839,"imageHeight":200,"dimensionWidth":839,"dimensionHeight":200,"imageSizes":{"full":{"height":200,"width":839,"url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/intro-animation2.svg","orientation":"portrait"},"medium":{"height":"300","width":"300","url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/intro-animation.svg","orientation":"portrait"},"thumbnail":{"height":"150","width":"150","url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/intro-animation.svg","orientation":"portrait"}}} /--></div></div>
<!-- /wp:cover -->

<!-- wp:acf/hero {"name":"acf/hero","data":{"hero_image":142,"_hero_image":"field_hero_hero_image","slogan":"  Versichern. Vertrauen. Machen.","_slogan":"field_hero_slogan"},"align":"full","mode":"preview"} -->
<!-- wp:group {"style":{"spacing":{"padding":{"right":"0","left":"0","top":"var:preset|spacing|70","bottom":"var:preset|spacing|80"}}},"layout":{"type":"flex","orientation":"vertical","justifyContent":"left","verticalAlignment":"center"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--70);padding-right:0;padding-bottom:var(--wp--preset--spacing--80);padding-left:0"><!-- wp:heading {"level":1,"placeholder":"Deine Überschrift","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"}}},"textColor":"teal"} -->
<h1 class="wp-block-heading has-teal-color has-text-color" style="margin-bottom:var(--wp--preset--spacing--30)">Große Verantwortung in guten Händen</h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"Deine Text","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--50)">Wichtige Entscheidungen brauchen Vertrauen. Und die Absicherung durch einen erfahrenen Partner, der im Geschäftsalltag den Rücken frei hält.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Kontakt aufnehmen</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->
<!-- /wp:acf/hero -->

<!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0","padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"backgroundColor":"teal","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-teal-background-color has-background" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)"><!-- wp:paragraph {"style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"top":"var:preset|spacing|40","bottom":"0"}}},"textColor":"white","fontSize":"base"} -->
<p class="has-white-color has-text-color has-base-font-size" style="margin-top:var(--wp--preset--spacing--40);margin-bottom:0;text-transform:uppercase">Lorem Ipsum</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"lineHeight":"1.4"},"spacing":{"margin":{"bottom":"var:preset|spacing|40"}}},"textColor":"white","fontSize":"4xl"} -->
<p class="has-white-color has-text-color has-4-xl-font-size" style="margin-bottom:var(--wp--preset--spacing--40);line-height:1.4">VVM ist das Maklerbüro für Menschen in Verantwortung. Mit klarem Fokus auf umfassende geschäftliche Absicherung. Wir schaffen Transparenz auf einem unübersichtlichen Markt und sind die verlässliche Begleitung durch alle Höhen und Tiefen des unternehmerischen Wegs.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"bottom":"var:preset|spacing|80"}}},"backgroundColor":"white","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="padding-bottom:var(--wp--preset--spacing--80)"><!-- wp:paragraph {"align":"center","style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"top":"0","bottom":"var:preset|spacing|20"}}},"textColor":"teal","fontSize":"base"} -->
<p class="has-text-align-center has-teal-color has-text-color has-base-font-size" style="margin-top:0;margin-bottom:var(--wp--preset--spacing--20);text-transform:uppercase">Für Menschen In Verantwortung</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","style":{"spacing":{"margin":{"top":"0","bottom":"var:preset|spacing|50"}}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color" style="margin-top:0;margin-bottom:var(--wp--preset--spacing--50)">Unsere Spezialexpertise</h2>
<!-- /wp:heading -->

<!-- wp:columns {"style":{"spacing":{"padding":{"top":"0","bottom":"0"}}},"className":"!grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6"} -->
<div class="wp-block-columns !grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6" style="padding-top:0;padding-bottom:0"><!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}},"layout":{"type":"default"}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"amazon-virtual-private-cloud-protect","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Cyber Versicherung","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"coding-apps-website-privacy-protection-shield-2","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"D\u0026O Versicherung","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"love-it-break","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":" Vertrauensschadenversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"ringe","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Warenkreditversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"sea-transport-boat","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Schiffsversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"protection-shield-4","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Versicherung Name","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"yellow-light","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull has-yellow-light-background-color has-background"><!-- wp:heading {"textAlign":"center","textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color">Unser Leistungsversprechen</h2>
<!-- /wp:heading -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"},"blockGap":{"top":"0","left":"0"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column -->

<!-- wp:column {"width":"80%"} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:paragraph {"align":"center","textColor":"teal","fontSize":"xl"} -->
<p class="has-text-align-center has-teal-color has-text-color has-xl-font-size">Verbindlichkeit sorgt für Verlässlichkeit. Unser Leistungsversprechen ist das verbindliche Angebot, das wir machen und unseren Kund:innen jeden Tag aufs Neue erfüllen.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"className":"cards"} -->
<div class="wp-block-columns cards" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"black","layout":{"type":"flex","orientation":"vertical","verticalAlignment":"top"}} -->
<div class="wp-block-group has-black-background-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"fontStyle":"normal","fontWeight":"900","textTransform":"none"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"textColor":"white","fontSize":"lg"} -->
<p class="has-white-color has-text-color has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"pinkie-finger","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview","style":{"layout":{"selfStretch":"fit","flexSize":null}}} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"teal","textColor":"white","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-white-color has-teal-background-color has-text-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"fontStyle":"normal","fontWeight":"900","textTransform":"none"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"fontSize":"lg"} -->
<p class="has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr,</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"reward-stars-2","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100%"} -->
<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"border":{"radius":"20px"}},"backgroundColor":"blue","layout":{"type":"flex","orientation":"vertical","justifyContent":"left"}} -->
<div class="wp-block-group has-blue-background-color has-background" style="border-radius:20px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)"><!-- wp:heading {"level":4,"style":{"typography":{"textTransform":"none","fontStyle":"normal","fontWeight":"900"},"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|50"}}},"textColor":"white","fontFamily":"satoshi"} -->
<h4 class="wp-block-heading has-white-color has-text-color has-satoshi-font-family" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--50);font-style:normal;font-weight:900;text-transform:none">Dies ist das Versprechen Nummer eins.</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"layout":{"selfStretch":"fill","flexSize":null}},"textColor":"white","fontSize":"lg"} -->
<p class="has-white-color has-text-color has-lg-font-size">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, dolor sit amet, consetetur sadipscing elitr.</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"100px","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"heart-approve-2","_icon":"field_icon_icon","background_color":"","_background_color":"field_icon_background_color","text_color":"","_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","width":"32","_width":"field_icon_width","auto-width":"0","_auto-width":"field_icon_auto-width","caption":"","_caption":"field_icon_caption","link":"","_link":"field_icon_link"},"align":"","mode":"preview","style":{"spacing":{"margin":{"right":"var:preset|spacing|20","left":"var:preset|spacing|20"}}}} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"},"blockGap":{"top":"0","left":"0"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column -->

<!-- wp:column {"width":"80%"} -->
<div class="wp-block-column" style="flex-basis:80%"><!-- wp:paragraph {"align":"center","textColor":"teal","fontSize":"xl"} -->
<p class="has-text-align-center has-teal-color has-text-color has-xl-font-size">Transparenz im Handeln und eine offene, verbindliche Kommunikation sind die Grundlage einer gesunden Zusammenarbeit. Wie dieser Ansatz zu spürbarem Mehrwert für unsere Kund:innen wird, haben wir hier gesondert umrissen:</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"10%"} -->
<div class="wp-block-column" style="flex-basis:10%"></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"yellow","textColor":"teal"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-teal-color has-yellow-background-color has-text-color has-background wp-element-button">Mehr dazu</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></section>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"bottom":"var:preset|spacing|80"}}},"backgroundColor":"white","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="padding-bottom:var(--wp--preset--spacing--80)"><!-- wp:heading {"textAlign":"center","style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color" style="margin-bottom:var(--wp--preset--spacing--50)">Fokusthemen</h2>
<!-- /wp:heading -->

<!-- wp:acf/postcarousel {"name":"acf/postcarousel","data":{"field_postcarousel_post_type":"focustopic","field_postcarousel_num-posts":"6 Beiträge"},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->

<!-- wp:group {"gradient":"whilte-to-teal","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-whilte-to-teal-gradient-background has-background"><!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|80"}}}} -->
<div class="wp-block-columns"><!-- wp:column {"width":"35%"} -->
<div class="wp-block-column" style="flex-basis:35%"><!-- wp:heading {"style":{"typography":{"textTransform":"uppercase"}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-teal-color has-text-color" style="text-transform:uppercase">Kontaktieren Sie uns</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"teal","fontSize":"xl"} -->
<p class="has-teal-color has-text-color has-xl-font-size">Sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0","bottom":"0"}}}} -->
<div class="wp-block-column" style="padding-top:0;padding-bottom:0"><!-- wp:contact-form-7/contact-form-selector {"id":175,"hash":"ac42cea","title":"Kontakformular"} -->
<div class="wp-block-contact-form-7-contact-form-selector">[contact-form-7 id="ac42cea" title="Kontakformular"]</div>
<!-- /wp:contact-form-7/contact-form-selector --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:group {"align":"full","backgroundColor":"transparent","textColor":"white","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-color has-transparent-background-color has-text-color has-background"><!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|60"}}}} -->
<h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--60)">Häufig gefragt</h2>
<!-- /wp:heading -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|white","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--white);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|white","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--white);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|white","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--white);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details -->

<!-- wp:details {"summary":"Lorem ipsum dolor sit amet, consetetur sadipscing?","style":{"border":{"top":{"width":"0px","style":"none"},"right":{"width":"0px","style":"none"},"bottom":{"color":"var:preset|color|white","width":"1px"},"left":{"width":"0px","style":"none"}}}} -->
<details class="wp-block-details" style="border-top-style:none;border-top-width:0px;border-right-style:none;border-right-width:0px;border-bottom-color:var(--wp--preset--color--white);border-bottom-width:1px;border-left-style:none;border-left-width:0px"><summary>Lorem ipsum dolor sit amet, consetetur sadipscing?</summary><!-- wp:paragraph {"placeholder":"Gib / ein, um einen verborgenen Block hinzuzufügen","style":{"spacing":{"padding":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}},"fontSize":"lg"} -->
<p class="has-lg-font-size" style="padding-top:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30)">Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
<!-- /wp:paragraph --></details>
<!-- /wp:details --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:cover {"url":"http://vvm.meta-maniacs.de/app/uploads/2023/10/Gruppenbild-SZ-SB-VV-DH.jpg","id":731,"dimRatio":20,"focalPoint":{"x":0.48,"y":0.29},"minHeight":350,"isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|80","bottom":"var:preset|spacing|80"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-cover alignfull is-light" style="padding-top:var(--wp--preset--spacing--80);padding-bottom:var(--wp--preset--spacing--80);min-height:350px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-731" alt="" src="http://vvm.meta-maniacs.de/app/uploads/2023/10/Gruppenbild-SZ-SB-VV-DH.jpg" style="object-position:48% 29%" data-object-fit="cover" data-object-position="48% 29%"/><div class="wp-block-cover__inner-container"><!-- wp:heading {"textAlign":"center","style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"bottom":"var:preset|spacing|50","top":"var:preset|spacing|70"}}},"textColor":"yellow-default","fontSize":"7xl"} -->
<h2 class="wp-block-heading has-text-align-center has-yellow-default-color has-text-color has-7-xl-font-size" style="margin-top:var(--wp--preset--spacing--70);margin-bottom:var(--wp--preset--spacing--50);text-transform:uppercase">Wir sind VVM</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"},"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|70"}}}} -->
<div class="wp-block-buttons" style="margin-bottom:var(--wp--preset--spacing--70)"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">Mehr über uns erfahren</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div></div>
<!-- /wp:cover -->