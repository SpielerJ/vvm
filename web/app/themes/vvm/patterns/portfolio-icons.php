<?php
/**
 * Title: Icon Portfolio
 * Slug: vvm/portfolio-icons
 * Categories: portfolio
 * Description: 5-spaltiges Iconset
 * Keywords: icons, portfolio, block, custom
 * Block Types: core/heading, core/columns, acf/icon,
 *
 * @see https://wordpress.stackexchange.com/a/398395/134384
 * @see https://fullsiteediting.com/lessons/introduction-to-block-patterns/#h-registering-block-patterns-using-the-patterns-folder
 */
?>

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"bottom":"var:preset|spacing|80"}}},"backgroundColor":"white","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-white-background-color has-background" style="padding-bottom:var(--wp--preset--spacing--80)"><!-- wp:paragraph {"align":"center","style":{"typography":{"textTransform":"uppercase"},"spacing":{"margin":{"top":"0","bottom":"var:preset|spacing|20"}}},"textColor":"teal","fontSize":"base"} -->
<p class="has-text-align-center has-teal-color has-text-color has-base-font-size" style="margin-top:0;margin-bottom:var(--wp--preset--spacing--20);text-transform:uppercase">Für Menschen In Verantwortung</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","style":{"spacing":{"margin":{"top":"0","bottom":"var:preset|spacing|50"}}},"textColor":"teal"} -->
<h2 class="wp-block-heading has-text-align-center has-teal-color has-text-color" style="margin-top:0;margin-bottom:var(--wp--preset--spacing--50)">Unsere Spezialexpertise</h2>
<!-- /wp:heading -->

<!-- wp:columns {"style":{"spacing":{"padding":{"top":"0","bottom":"0"}}},"className":"!grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5"} -->
<div class="wp-block-columns !grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5" style="padding-top:0;padding-bottom:0"><!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}},"layout":{"type":"default"}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"amazon-virtual-private-cloud-protect","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Cyber Versicherung","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"coding-apps-website-privacy-protection-shield-2","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"D\u0026O Versicherung","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"love-it-break","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":" Vertrauensschadenversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"ringe","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Warenkreditversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|40"}}}} -->
<div class="wp-block-column" style="padding-bottom:var(--wp--preset--spacing--40)"><!-- wp:acf/icon {"name":"acf/icon","data":{"icon":"sea-transport-boat","_icon":"field_icon_icon","background_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_background_color":"field_icon_background_color","text_color":{"color":"#014953","name":"Teal","slug":"teal","text":"has-text-color has-teal-color","background":"has-background has-teal-background-color"},"_text_color":"field_icon_text_color","icon_color":{"color":"#fff","name":"White","slug":"white","text":"has-text-color has-white-color","background":"has-background has-white-background-color"},"_icon_color":"field_icon_icon_color","auto-width":"1","_auto-width":"field_icon_auto-width","caption":"Schiffsversicherung ","_caption":"field_icon_caption","link":{"title":"Cyberversicherung","url":"http://vvm.meta-maniacs.de/products/cyberversicherung/","target":""},"_link":"field_icon_link"},"align":"","mode":"preview"} /--></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->