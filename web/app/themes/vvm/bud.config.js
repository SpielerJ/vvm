import rfs from 'rfs';

/**
 * Compiler configuration
 *
 * @see {@link https://roots.io/docs/sage sage documentation}
 * @see {@link https://bud.js.org/guides/configure bud.js configuration guide}
 *
 * @param {import('@roots/bud').Bud} app
 */
export default async (app) => {
  /**
   * Application assets & entrypoints
   *
   * @see {@link https://bud.js.org/docs/bud.entry}
   * @see {@link https://bud.js.org/docs/bud.assets}
   */
  app
    .entry('app', ['@scripts/app', '@styles/app'])
    .entry('editor', ['@scripts/editor', '@styles/editor'])
    .assets(['images']);

  /**
   * The following specifies that $ is the default jQuery export:
   * Now, in any module in our application, we can invoke jQuery with $. There is no need to import it.
   */
  app.provide({
    jquery: '$',
  });

  /**
   * Set public path
   *
   * @see {@link https://bud.js.org/docs/bud.setPublicPath}
   */
  app.setPublicPath('/app/themes/vvm/public/');

  /**
   * Development server settings
   *
   * @see {@link https://bud.js.org/docs/bud.setUrl}
   * @see {@link https://bud.js.org/docs/bud.setProxyUrl}
   * @see {@link https://bud.js.org/docs/bud.watch}
   */
  app
    .setUrl('http://localhost:3000')
    .setProxyUrl('https://vvm.ddev.site')
    .watch(['resources/views', 'app']);

  /*
    rfs
  */

  app.postcss.setPlugin('rfs', [
    'rfs',
    {
      twoDimensional: false,
      baseValue: 16,
      unit: 'rem',
      breakpoint: 1320,
      breakpointUnit: 'px',
      factor: 5,
      class: false,
      unitPrecision: 6,
      safariIframeResizeBugFix: false,
      remValue: 16
    }
  ]);

  app.postcss.use([
    'import', 'nesting', 'tailwindcss', 'env', 'rfs'
  ])

  /**
   * Generate WordPress `theme.json`
   *
   * @note This overwrites `theme.json` on every build.
   *
   * @see {@link https://bud.js.org/extensions/sage/theme.json}
   * @see {@link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json}
   */
  app.wpjson
    .set('settings.useRootPaddingAwareAlignments', true)
    .set('settings.layout.contentSize', "90%")
    .set('settings.layout.type', "constrained")
    .set('settings.position.sticky', true)
    .set('settings.color.custom', false)
    .set('settings.color.customDuotone', false)
    .set('settings.color.customGradient', false)
    .set('settings.color.defaultDuotone', false)
    .set('settings.color.defaultGradients', false)
    .set('settings.color.defaultPalette', false)
    .set('settings.color.duotone', [])
    .set('settings.color.gradients', [
      {
        "slug": "whilte-to-teal",
        "gradient": "linear-gradient(to bottom,var(--wp--preset--color--white) 0% ,var(--wp--preset--color--teal) 100%)",
        "name": "Vertical white to teal"
      }
    ])
    .set('settings.spacing.padding', true)
    .set('settings.spacing.margin', true)
    .set('settings.spacing.customSpacingSize', false)
    .set('settings.spacing.blockGap', false)
    /*.set('settings.spacing.spacingScale', {
      "operator": "*",
      "increment": 1.5,
      "steps": 8,
      "mediumStep": 1.5,
      "unit": "rem"
    })*/
    .set('settings.spacing.spacingSizes', [
        {
          "size": "clamp(0.49383rem, calc(12.290vw / 15.3489), 0.7681755rem)",
          "slug": "20",
          "name": "2"
        },
        {
          "size": "clamp(0.74074rem, calc(18.43621vw / 15.3489), 1.15226rem)",
          "slug": "30",
          "name": "3"
        },
        {
          "size": "clamp(1.111rem, calc(27.65432vw / 15.3489), 1.72839rem)",
          "slug": "40",
          "name": "4" 
        },
        {
          "size": "clamp(1.667rem, calc(41.48148vw / 15.3489), 2.59259rem)",
          "slug": "50",
          "name": "5"
        },
        {
          "size": "clamp(2.5rem, calc(62.22vw / 15.3489), 3.88889rem)",
          "slug": "60",
          "name": "6"
        },
        {
          "size": "clamp(3.75rem, calc(93.33vw / 15.3489), 5.83333rem)",
          "slug": "70",
          "name": "7"
        },
        {
          "size": "clamp(5.625rem, calc(140vw / 15.3489), 8.75rem)",
          "slug": "80",
          "name": "8"
        },
      ]
    )
    .set('settings.spacing.units', ['px', '%', 'em', 'rem', 'vw', 'vh'])
    .set('settings.typography.customFontSize', false)
    .set('settings.typography.fontStyle', false)
    .set('settings.typography.textDecoration', false)
    .set('settings.typography.dropCap', false)
    .set('settings.typography.letterSpacing', false)
    .set('settings.typography.fontFamilies',
      [
        {
          fontFamily: "\"Big Shoulders Display\", serif",
          name: "Big Shoulders Display",
          slug: "big-shoulders-display",
          fontFace: [{
            fontWeight: "800",
            fontFamily: "Big Shoulders Display",
            src: ["file:./resources/fonts/big-shoulders-display-v21-latin-800.woff2"],
          }],
          fontFace: [{
            fontWeight: "900",
            fontFamily: "Big Shoulders Display",
            src: ["file:./resources/fonts/big-shoulders-display-v21-latin-900.woff2"],
          }]
        },
        {
          fontFamily: "\"Satoshi\", sans-serif",
          name: "Satoshi",
          slug: "satoshi",
          fontFace: [
            {
              fontWeight: "500",
              fontStyle: "normal",
              fontFamily: "Satoshi-Medium",
              src: ["file:./resources/fonts/Satoshi-Medium.woff2"],
            },
            {
              fontWeight: "700",
              fontStyle: "normal",
              fontFamily: "Satoshi-Bold",
              src: ["file:./resources/fonts/Satoshi-Bold.woff2"],
            },
            {
              fontWeight: "900",
              fontStyle: "normal",
              fontFamily: "Satoshi-Black",
              src: ["file:./resources/fonts/Satoshi-Black.woff2"],
            }
          ]
        }
      ]
    )
    .set('settings.blocks.core/button.border.radius', false)
    .set('settings.blocks.core/button.color.text', false)
    .set('settings.blocks.core/button.color.background', false)
    .set('settings.blocks.core/button.typography.textTransform', false)
    .set('settings.blocks.core/button.typography.fontWeight', false)
    .set('settings.blocks.core/button.typography.fontFamilies', [])
    .set('settings.blocks.core/button.typography.fontSizes', [])
    .set('settings.blocks.core/button.spacing.padding', false)
    .set('settings.blocks.core/group.border.radius', true)
    .set('settings.blocks.core/group.border.customRadius', false)
    .set('settings.blocks.core/group.color.overlay', true)
    .set('settings.blocks.core/paragraph.spacing.margin', true)
    .set('settings.blocks.core/paragraph.typography.lineHeight', true)
    .set('settings.blocks.core/details.border.color', true)
    .set('settings.blocks.core/columns.spacing.blockGap', true)
    .set('settings.blocks.core/group.spacing.blockGap', true)
    .set('settings.blocks.core/post-title.typography', true)
    .set('settings.blocks.core/post-title.color.text', true)
    .set('settings.blocks.core/post-title.spacing', true)
    
    /*
     * Styles
     */

    // generel
    .set('styles.color.text', "var(--wp--preset--color--teal)")
    .set('styles.spacing.blockGap', "0")


    .set('styles.typography.fontWeight', "500")
    .set('styles.typography.fontFamily', "var(--wp--preset--font-family--satoshi)")
    .set('styles.typography.lineHeight', "1.4")


    // core/comments-pagination
    .set('styles.blocks.core/comments-pagination.spacing.blockGap', "16px")


    // core/heading
    .set('styles.blocks.core/heading.typography.textTransform', 'uppercase')
    .set('styles.blocks.core/heading.typography.lineHeight', "1.1")
    .set('styles.blocks.core/heading.spacing.margin', {
      "left": "0", 
      "right": "0",
      "top": "var(--wp--preset--spacing--50)",
      "bottom": "var(--wp--preset--spacing--30)"
    })

    // core/quote
    .set('styles.blocks.core/quote.color.text', 'var(--wp--preset--color--yellow-default)')
    .set('styles.blocks.core/quote.typography.textTransform', 'uppercase')
    .set('styles.blocks.core/quote.typography.lineHeight', "1.1")
    .set('styles.blocks.core/quote.typography.fontStyle', "normal")

    // core/post-title
    .set('styles.blocks.core/post-title.typography.textTransform', 'uppercase')
    .set('styles.blocks.core/post-title.typography.lineHeight', "1.1")
    .set('styles.blocks.core/post-title.typography.fontFamily', "var(--wp--preset--font-family--big-shoulders-display)")
    .set('styles.blocks.core/post-title.spacing.margin', {
      "left": "0", 
      "right": "0",
      "top": "var(--wp--preset--spacing--40)",
      "bottom": "var(--wp--preset--spacing--40)"
    })

    // core/quote
    .set('styles.blocks.core/quote.spacing.margin', {
      "left": "40px", 
      "right": "40px"
    })

    // core/paragraph
    .set('styles.blocks.core/paragraph.spacing.margin', {
      "top": "0",
      "bottom": "clamp(0.32922rem, calc(8.19333vw / 15.3489), 0.512117rem)"
    })

    // core/columns
    .set('styles.blocks.core/columns.spacing.blockGap', "16px")

    // core/site-title
    .set('styles.blocks.core/site-title.typography.fontFamily', "var(--wp--preset--font-family--big-shoulders-display)")
    .set('styles.blocks.core/site-title.typography.fontWeight', "900")

    // core/button
    .set('styles.blocks.core/buttons.spacing.blockGap', "var(--wp--preset--spacing--20)")
    .set('styles.blocks.core/button.border.radius', "0px")
    .set('styles.blocks.core/button.typography.textTransform', "uppercase")
    .set('styles.blocks.core/button.typography.fontFamily', "var(--wp--preset--font-family--satoshi)")
    .set('styles.blocks.core/button.typography.fontWeight', "700")
    .set('styles.blocks.core/button.typography.lineHeight', "1")
    .set('styles.blocks.core/button.color.background', "var(--wp--preset--color--yellow-default)")
    .set('styles.blocks.core/button.color.text', "var(--wp--preset--color--teal)")
    .set('styles.blocks.core/button.spacing.padding', {
      "left": "1rem", 
      "right": "1rem",
      "top": "1rem", 
      "bottom": "1rem"
    })

    // core/group
    .set('styles.blocks.core/group.border.radius', "0px")
    .set('styles.blocks.core/group.spacing.padding', {
      "left": "0", 
      "right": "0",
      "top": "var(--wp--preset--spacing--80)", 
      "bottom": "var(--wp--preset--spacing--80)"
    })
    .set('styles.blocks.core/group.dimensions.minHeight', "100%")
    .set('styles.blocks.core/group.color.background', "var(--wp--preset--color--white)")
    .set('styles.blocks.core/columns.spacing.blockGap', "16px")

    // core/cover
    .set('styles.blocks.core/cover.spacing.padding', {
      "left": "0", 
      "right": "0",
      "top": "var(--wp--preset--spacing--80)", 
      "bottom": "var(--wp--preset--spacing--80)"
    })

    // core/details
    .set('styles.blocks.core/details.border.color', "var(--wp--preset--color--teal)")
    .set('styles.blocks.core/details.border.width', "0 0 1px 0")
    .set('styles.blocks.core/details.spacing.padding', {
      "top": "var(--wp--preset--spacing--30)",
      "right": "0px",
      "bottom": "var(--wp--preset--spacing--30)",
      "left": "0px"
    })

    // acf/icon
    .set('styles.blocks.acf/icon.color.background', "var(--wp--preset--color--transparent)")

    // acf/employeecontact
    .set('styles.blocks.acf/employeecontact.color.text', "var(--wp--preset--color--teal)")

    .setOption('templateParts', [
      {
        "name": "header",
        "title": "Header",
        "area": "header",
      },

      {
        "name": "footer",
        "title": "Footer",
        "area": "footer",
      },

      {
        "name": "cards",
        "title": "Cards",
        "area": "uncategorized", // (empty default is `uncategorized`)
      },
    ])

    .setOption('customTemplates', [
      {
        "name":  "page-without-header",
        "title": "Page without header",
        // description currently not supported (@see https://github.com/WordPress/gutenberg/issues/44097)
      },
    ])
    
    .useTailwindColors()
    .useTailwindFontSize()
    .enable();
};
