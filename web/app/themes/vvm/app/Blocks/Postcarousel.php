<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use Roots\Acorn\Application;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Postcarousel extends Block
{
    public function __construct(Application $app)
    {
        /**
         * The block name.
         *
         * @var string
         */
        $this->name = __('Block Beitragskarussell', 'sage');

        /**
         * The block slug.
         *
         * @var string
         */
        $this->slug = 'postcarousel';

        /**
         * The block description.
         *
         * @var string
         */
        $this->description = __('Zeigt ein Karrussel mit den neusten Beiträgen', 'sage');

        /**
         * The block category.
         *
         * @var string
         */
        $this->category = 'vvm';

        /**
         * The block icon.
         *
         * @var string|array
         */
        $this->icon = 'slides';

        /**
         * The block keywords.
         *
         * @var array
         */
        $this->keywords = [];

        /**
         * The block post type allow list.
         *
         * @var array
         */
        $this->post_types = [];

        /**
         * The parent block type allow list.
         *
         * @var array
         */
        $this->parent = [];

        /**
         * The default block mode.
         *
         * @var string
         */
        $this->mode = 'preview';

        /**
         * The default block alignment.
         *
         * @var string
         */
        $this->align = '';

        /**
         * The default block text alignment.
         *
         * @var string
         */
        $this->align_text = '';

        /**
         * The default block content alignment.
         *
         * @var string
         */
        $this->align_content = '';

        /**
         * The supported block features.
         *
         * @var array
         */
        $this->supports = [
            'align' => true,
            'align_text' => false,
            'align_content' => false,
            'full_height' => false,
            'anchor' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => false,
            'spacing' => [
                'margin' => true,
                'padding' => true,
            ]
        ];

        /**
         * The block preview example data.
         *
         * @var array
         */
        $this->example = [
            ['post_type' => 'focustopic'],
            ['num-posts' => (int)6],
        ];

        parent::__construct($app);
    }

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'items' => $this->items(),
            'query' => $this->queryPosts(),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $postcarousel = new FieldsBuilder('postcarousel');

        $postcarousel
            ->addSelect('post_type', [
                'label' => 'Beitragstyp wählen',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'posts',
                            'operator' => '==empty',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'choices' => [
                    'post' => 'Beiträge',
					'focustopic' => 'Fokusthemen',
					'product' => 'Produkte',
                ],
                'default_value' => [],
                'allow_null' => 1,
                'multiple' => 0,
                'ui' => 1,
                'ajax' => 0,
                'return_format' => 'value',
                'placeholder' => '',
            ])
            ->addSelect('num-posts', [
                'label' => 'Anzahl der Beiträge',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'post_type',
                            'operator' => '!=empty',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'choices' => [
                    '-1' => 'alle Beiträge',
                    3 => '3 Beiträge',
					6 => '6 Beiträge',
					9 => '9 Beiträge',
                ],
				'default_value' => '-1',
				'return_format' => 'value',
				'multiple' => 0,
				'allow_null' => 0,
				'ui' => 1,
				'ajax' => 0,
				'placeholder' => '',
            ])
            ->addPostObject('posts', [
                'label' => 'benutzerdefinierte Beiträge',
                'instructions' => 'Inhalt wählen',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'post_type',
                            'operator' => '==empty',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => ['post', 'focustopic', 'product'],
                'taxonomy' => [],
                'allow_null' => 1,
                'multiple' => 1,
                'return_format' => 'id',
                'ui' => 1,
            ]);

        return $postcarousel->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function items()
    {
        return get_fields() ?: $this->example;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }

    /**
     * Return the query.
     *
     * @return array
     */
    public function queryPosts()
    {
        $postType = get_field('post_type');
        $numPosts = (int)get_field('num-posts');
        $postIds = get_field('posts');
        
        if ($postIds) {
            $args = [
                'post_type' => 'employee',
                'post__in' => $postIds,
                'post_type' => 'any',
                'orderby' => 'post__in',
                'ignore_sticky_posts' => 1,
            ];
        }
        else {
            $args = array();
            $args['post_type'] = $postType;
            $args['posts_per_page'] = $numPosts;
        }
        $query = new \WP_Query($args);
        wp_reset_postdata();
        return $query;

    }
}
