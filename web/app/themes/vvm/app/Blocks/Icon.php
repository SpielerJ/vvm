<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use Roots\Acorn\Application;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Icon extends Block
{
    public function __construct(Application $app)
    {
        /**
         * The block name.
         *
         * @var string
         */
        $this->name = __('Icon', 'sage');

        /**
         * The block slug.
         *
         * @var string
         */
        $this->slug = 'icon';

        /**
         * The block description.
         *
         * @var string
         */
        $this->description = __('Wähle aus einer Liste von Icons', 'sage');

        /**
         * The block category.
         *
         * @var string
         */
        $this->category = 'vvm';

        /**
         * The block icon.
         *
         * @var string|array
         */
        $this->icon = 'star-half';

        /**
         * The block keywords.
         *
         * @var array
         */
        $this->keywords = [];

        /**
         * The block post type allow list.
         *
         * @var array
         */
        $this->post_types = [];

        /**
         * The parent block type allow list.
         *
         * @var array
         */
        $this->parent = [];

        /**
         * The default block mode.
         *
         * @var string
         */
        $this->mode = 'preview';

        /**
         * The default block alignment.
         *
         * @var string
         */
        $this->align = '';

        /**
         * The default block text alignment.
         *
         * @var string
         */
        $this->align_text = '';

        /**
         * The default block content alignment.
         *
         * @var string
         */
        $this->align_content = '';

        /**
         * The supported block features.
         *
         * @var array
         */
        $this->supports = [
            'align' => true,
            'align_text' => false,
            'align_content' => false,
            'full_width' => false,
            'anchor' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => false,
            'full_height' => false,
            'spacing' => [
                'margin' => true,
                'padding' => false,
            ]
        ];

        /**
         * The block preview example data.
         *
         * @var array
         */
        $this->example = [
            'icon' => 'accessible',
            'width' => '48',
        ];

        parent::__construct($app);
    }


    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'item' => $this->items(),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $icon = new FieldsBuilder('icon');

        $icon
            ->addField('icon', 'icon-picker')
                ->setLabel('Icon')
                ->setInstructions('Wähle ein Icon')
                ->setRequired(1)
            ->addField('background_color', 'editor_palette')
                ->setLabel('Hintergrundfarbe')
                ->setConfig('allowed_colors', ['white', 'blue', 'black', 'yellow', 'teal'])
                ->setConfig('return_format', 'slug')
            ->addField('text_color', 'editor_palette')
                ->setLabel('Textfarbe')
                ->setConfig('default_value', 'teal')
                ->setConfig('allowed_colors', ['white', 'blue', 'black', 'yellow', 'teal'])
                ->setConfig('return_format', 'slug')
            ->addField('icon_color', 'editor_palette')
                ->setLabel('Iconfarbe')
                ->setConfig('default_value', 'black')
                ->setConfig('allowed_colors', [ 'white', 'blue', 'black', 'yellow', 'teal'])
                ->setConfig('return_format', 'slug')
            ->addTrueFalse('auto-width', [
                    'label' => 'Größe automatisch anpassen',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => [],
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 1,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ])
            ->addRange('width', [
                'label' => 'Icon Größe',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'auto-width',
                            'operator' => '!=',
                            'value' => '1',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '48',
                'min' => '24',
                'max' => '96',
                'step' => '8',
                'prepend' => '',
                'append' => '',
            ])
            ->addText('caption', [
                'label' => 'Beschriftung',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ])
            ->addLink('link', [
                'label' => 'Link',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'array',
            ]);
        
        return $icon->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function items()
    {
        return get_fields() ?: $this->example;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }
}
