<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Megamenu extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Megamenu';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Megamenu block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The ancestor block type allow list.
     *
     * @var array
     */
    public $ancestor = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'full_height' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
        'color' => [
            'background' => true,
            'text' => true,
            'gradient' => true,
        ],
        'spacing' => [
            'margin' => true,
            'padding' => true,
        ]
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
        [
            'name' => 'light',
            'label' => 'Light',
            'isDefault' => true,
        ],
        [
            'name' => 'dark',
            'label' => 'Dark',
        ]
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
        'items' => [
            ['item' => 'Item one'],
            ['item' => 'Item two'],
            ['item' => 'Item three'],
        ],
    ];

    /**
     * The block template.
     *
     * @var array
     */
    public $template = [
        'core/heading' => ['placeholder' => 'Hello World'],
        'core/paragraph' => ['placeholder' => 'Welcome to the Megamenu block.'],
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'items' => $this->items(),
            'menu'  => $this->getMenu(),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $megamenu = new FieldsBuilder('megamenu');

        $megamenu
            ->addTaxonomy('menu', [
                'label' => 'Menu',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'taxonomy' => 'nav_menu',
                'field_type' => 'select',
                'allow_null' => 0,
                'add_term' => 0,
                'save_terms' => 0,
                'load_terms' => 0,
                'return_format' => 'id',
                'multiple' => 0,
            ]);

        return $megamenu->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function items()
    {
        return wp_get_nav_menu_items(get_field('menu')) ?: $this->example['items'];
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }

        /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function getMenu()
    {
        $menu_array = wp_get_nav_menu_items(get_field('menu'));
        $menu = array();
        function populate_children($menu_array, $menu_item)
        {
            $children = array();
            if (!empty($menu_array)){
                foreach ($menu_array as $k=>$m) {
                    if ($m->menu_item_parent == $menu_item->ID) {
                        $children[$m->ID] = array();
                        $children[$m->ID]['ID'] = $m->ID;
                        $children[$m->ID]['title'] = wp_specialchars_decode($m->title);
                        $children[$m->ID]['url'] = $m->url;
                        $children[$m->ID]['target'] = $m->target;
                        $children[$m->ID]['description'] = $m->description;
                        $children[$m->ID]['classes'] = $m->classes;
                        unset($menu_array[$k]);
                        $children[$m->ID]['children'] = populate_children($menu_array, $m);
                        $children[$m->ID]['object_id'] = $m->object_id;
                    }
                }
            };
            return $children;
        }
    
        foreach ($menu_array as $m) {
            if (empty($m->menu_item_parent)) {
                $menu[$m->ID] = array();
                $menu[$m->ID]['ID'] = $m->ID;
                $menu[$m->ID]['title'] = wp_specialchars_decode($m->title);
                $menu[$m->ID]['url'] = $m->url;
                $menu[$m->ID]['target'] = $m->target;
                $menu[$m->ID]['description'] = $m->description;
                $menu[$m->ID]['classes'] = $m->classes;
                $menu[$m->ID]['children'] = populate_children($menu_array, $m);
                $menu[$m->ID]['object_id'] = $m->object_id;
            }
        }
        return $menu;
    }

}
