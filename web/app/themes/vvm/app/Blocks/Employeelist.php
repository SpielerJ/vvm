<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use Roots\Acorn\Application;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Employeelist extends Block
{
    public function __construct(Application $app)
    {
        /**
         * The block name.
         *
         * @var string
         */
        $this->name = __('Mitarbeiterliste', 'sage');

        /**
         * The block slug.
         *
         * @var string
         */
        $this->slug = 'employeelist';

        /**
         * The block description.
         *
         * @var string
         */
        $this->description = __('A simple Employeelist block.', 'sage');

        /**
         * The block category.
         *
         * @var string
         */
        $this->category = 'vvm';

        /**
         * The block icon.
         *
         * @var string|array
         */
        $this->icon = 'businessperson';

        /**
         * The block keywords.
         *
         * @var array
         */
        $this->keywords = [];

        /**
         * The block post type allow list.
         *
         * @var array
         */
        $this->post_types = [];

        /**
         * The parent block type allow list.
         *
         * @var array
         */
        $this->parent = [];

        /**
         * The default block mode.
         *
         * @var string
         */
        $this->mode = 'preview';

        /**
         * The default block alignment.
         *
         * @var string
         */
        $this->align = '';

        /**
         * The default block text alignment.
         *
         * @var string
         */
        $this->align_text = '';

        /**
         * The default block content alignment.
         *
         * @var string
         */
        $this->align_content = '';

        /**
         * The supported block features.
         *
         * @var array
         */
        $this->supports = [
            'align' => true,
            'align_text' => false,
            'align_content' => false,
            'full_height' => false,
            'anchor' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => false,
            'spacing' => [
                'margin' => true,
                'padding' => true,
            ]
        ];

        /**
         * The block preview example data.
         *
         * @var array
         */
        $this->example = [
           'items' => [
               ['item' => 'Item one'],
               ['item' => 'Item two'],
               ['item' => 'Item three'],
           ],
        ];

        parent::__construct($app);
    }

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'items' => $this->items(),
            'query' => $this->queryPosts()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $employeelist = new FieldsBuilder('employeelist');

        $employeelist
            ->addTaxonomy('profession-terms', [
                'label' => 'Tätigkeiten',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'employees',
                            'operator' => '==empty',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'taxonomy' => 'profession',
                'field_type' => 'checkbox',
                'allow_null' => 0,
                'add_term' => 0,
                'save_terms' => 0,
                'load_terms' => 0,
                'return_format' => 'id',
                'multiple' => 1,
            ])
            ->addPostObject('employees', [
                'label' => 'benutzerdefinierte Mitarbeiter:innen',
                'instructions' => 'Inhalt wählen',
                'required' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => 'profession-terms',
                            'operator' => '==empty',
                        ]
                    ]
                ],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => ['employee'],
                'taxonomy' => [],
                'allow_null' => 1,
                'multiple' => 1,
                'return_format' => 'id',
                'ui' => 1,
            ]);

        return $employeelist->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function items()
    {
        return get_fields() ?: $this->example;
    }
    
    /**
     * Return the items field.
     *
     * @return array
     */
    public function queryPosts()
    {
        $subjectTermIds = get_field('subject-terms');
        $professionTermIds = get_field('profession-terms');
        $postIds = get_field('employees');
        
        if ($postIds) {
            $args = [
                'post_type' => 'employee',
                'post__in' => $postIds,
                'orderby' => 'post__in',
                'ignore_sticky_posts' => 1,
            ];
        }
        else {

            $args = [
                'post_type' => 'employee',
                'posts_per_page' => -1,
                'orderby' => 'meta_value',
                'meta_key' => 'last-name',
                'order' => 'ASC',
            ];

            if ($professionTermIds) {
                $args['tax_query']['realation'] = 'AND';
                $args['tax_query'][] = [
                    'tax_query' => array(
                        'taxonomy'  => 'profession',
                        'field'  => 'term_id',
                        'terms'  => $professionTermIds,
                        'operator' => 'IN'
                    )
                ];
            }
        }

        $query = new \WP_Query($args);

        wp_reset_postdata();
        return $query;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }
}
