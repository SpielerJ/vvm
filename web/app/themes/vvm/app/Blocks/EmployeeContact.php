<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use Roots\Acorn\Application;
use StoutLogic\AcfBuilder\FieldsBuilder;

class EmployeeContact extends Block
{
    public function __construct(Application $app)
    {
        /**
         * The block name.
         *
         * @var string
         */
        $this->name = __('Mitarbeiter Kontakt', 'sage');

        /**
         * The block slug.
         *
         * @var string
         */
        $this->slug = 'employeecontact';

        /**
         * The block description.
         *
         * @var string
         */
        $this->description = __('A simple Employee block.', 'sage');

        /**
         * The block category.
         *
         * @var string
         */
        $this->category = 'vvm';

        /**
         * The block icon.
         *
         * @var string|array
         */
        $this->icon = 'id-alt';

        /**
         * The block keywords.
         *
         * @var array
         */
        $this->keywords = [];

        /**
         * The block post type allow list.
         *
         * @var array
         */
        $this->post_types = [];

        /**
         * The parent block type allow list.
         *
         * @var array
         */
        $this->parent = [];

        /**
         * The default block mode.
         *
         * @var string
         */
        $this->mode = 'preview';

        /**
         * The default block alignment.
         *
         * @var string
         */
        $this->align = '';

        /**
         * The default block text alignment.
         *
         * @var string
         */
        $this->align_text = '';

        /**
         * The default block content alignment.
         *
         * @var string
         */
        $this->align_content = '';

        /**
         * The supported block features.
         *
         * @var array
         */
        $this->supports = [
            'align' => true,
            'align_text' => false,
            'align_content' => false,
            'full_height' => false,
            'anchor' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => false,
            'spacing' => [
                'margin' => true,
                'padding' => true,
            ],
            'color' => [
                'background' => false,
                'text' => true,
            ],
        ];

        /**
         * The block preview example data.
         *
         * @var array
         */
        $this->example = [
            'employee' => (int)381,
            'heading' => 'Ihre Fachfrau für Cyberversicherung',
        ];

        parent::__construct($app);
    }

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'employee' => $this->employee(),
            'heading' => $this->heading(),
            'query' => $this->queryPosts($this->employee()),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $employeecontact = new FieldsBuilder('employeecontact');

        $employeecontact
            ->addPostObject('employee', [
                'label' => 'Mitarbeiter:in',
                'instructions' => 'Inhalt wählen',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => ['employee'],
                'taxonomy' => [],
                'allow_null' => 1,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ])
            ->addText('heading', [
                'label' => 'Überschrift',
                'instructions' => '',
                'required' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'placeholder' => 'Ihre Fachfrau für Cyberversicherung',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ]);

        return $employeecontact->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function employee()
    {
        return get_field('employee') ?: $this->example['employee'];
    }

        /**
     * Return the items field.
     *
     * @return array
     */
    public function heading()
    {
        return get_field('heading') ?: $this->example['heading'];
    }
    
    /**
     * Return the items field.
     *
     * @return array
     */
    public function queryPosts($employee)
    {   
        $postId = get_field('employee');
        
        if ($postId != false) {
            $args = [
                'p' => $postId,
                'post_type' => 'employee',
            ];
        }
        else {
            $args = [
                'p' => $employee,
                'post_type' => 'employee',
            ];
        }

        $query = new \WP_Query($args);

        wp_reset_postdata();
        return $query;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }
}
