<?php

namespace App\Blocks;

use Log1x\AcfComposer\Block;
use Roots\Acorn\Application;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Hero extends Block
{
    public function __construct(Application $app)
    {
        /**
         * The block name.
         *
         * @var string
         */
        $this->name = __('Hero', 'sage');

        /**
         * The block slug.
         *
         * @var string
         */
        $this->slug = 'hero';

        /**
         * The block description.
         *
         * @var string
         */
        $this->description = __('Ein großer Page-Hero', 'sage');

        /**
         * The block category.
         *
         * @var string
         */
        $this->category = 'vvm';

        /**
         * The block icon.
         *
         * @var string|array
         */
        $this->icon = 'align-pull-left';

        /**
         * The block keywords.
         *
         * @var array
         */
        $this->keywords = [];

        /**
         * The block post type allow list.
         *
         * @var array
         */
        $this->post_types = [];

        /**
         * The parent block type allow list.
         *
         * @var array
         */
        $this->parent = [];

        /**
         * The default block mode.
         *
         * @var string
         */
        $this->mode = 'preview';

        /**
         * The default block alignment.
         *
         * @var string
         */
        $this->align = 'full';

        /**
         * The default block text alignment.
         *
         * @var string
         */
        $this->align_text = '';

        /**
         * The default block content alignment.
         *
         * @var string
         */
        $this->align_content = '';

        /**
         * The supported block features.
         *
         * @var array
         */
        $this->supports = [
            'align' => true,
            'align_text' => false,
            'align_content' => false,
            'full_width' => true,
            'anchor' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => true,
            'full_height' => true,
        ];

        /**
         * The block preview example data.
         *
         * @var array
         */
        $this->example = [
            'hero_image' => '',
        ];

        parent::__construct($app);
    }


    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'item' => $this->items(),
            'allowed_blocks'    => $this->allowed_blocks(),
            'template'          => $this->block_template(),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $hero = new FieldsBuilder('hero');

        $hero
            ->addImage('hero_image', [
                'label' => 'Hero Bild',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'id',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ]);
        
        return $hero->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function items()
    {
        return get_fields() ?: $this->example;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }

    public function block_template() {
        return esc_attr(wp_json_encode([
            ['core/group',[
                    'layout' => [
                        'type' => 'constrained',
                    ],
                ],
                [
                    ['core/heading', [
                        'level'       => 1,
                        'placeholder' => 'Deine Überschrift',
                        'textColor' => 'teal',
                    ]],
                    ['core/paragraph', [
                        'placeholder' => 'Deine Text',
                        'fontSize' => 'xl',
                        'textColor' => 'teal',
                        'style' => [
                            'spacing' => [
                                'margin' => [
                                    'bottom' => 'var(--wp--preset--spacing--80)'
                                ]
                            ]
                        ]
                    ]],
                    ['core/buttons', [
                        array(),
                        array(
                            ['core/button', [
                                'placeholder' => 'Deine Text',
                            ]]
                        )
                    ]]
                ]
            ]
        ]));
      }
      
      public function allowed_blocks() {
          return esc_attr(wp_json_encode([
              'core/heading',
              'core/paragraph',
              'core/buttons',
              'core/button',
              'core/group',
          ]));
      }
}
