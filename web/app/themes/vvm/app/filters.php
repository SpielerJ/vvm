<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Adds "…" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return '&hellip;';
});

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
add_filter( 'excerpt_length', function ( $length ) {
	return 45;
});

/// modify the path to the icons directory
add_filter('acf_icon_path_suffix',
  function ( $path_suffix ) {
    return 'resources/images/icons/'; // After assets folder you can define folder structure
  }
);


// modify the path to the above prefix
add_filter('acf_icon_path',
  function ( $path_suffix ) {
    return get_stylesheet_directory();
  }
);

// modify the URL to the icons directory to display on the page
add_filter('acf_icon_url',
  function ( $path_suffix ) {
    return get_stylesheet_directory_uri();
  }
);

add_filter( 'wpcf7_autop_or_not', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

add_filter('wpcf7_form_elements', function($html){
  //wp_send_json($html );
  $text = 'Bitte auswählen';
  $html = str_replace('&#8211; Bitte auswählen &#8211;',  $text, $html);

  return $html;
});