<?php

namespace App\Fields;

use Log1x\AcfComposer\Field;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Employee extends Field
{
    /**
     * The field group.
     *
     * @return array
     */
    public function fields()
    {
        $employee = new FieldsBuilder('employee');

        $employee
            ->setLocation('post_type', '==', 'employee');

        $employee
            ->addText('title',[
                'label' => 'Titel'
            ])
            ->addText('first-name',[
                'label' => 'Vorname'
            ])
            ->addText('last-name',[
                'label' => 'Nachname',
                'required' => '1'
            ])
            ->addEmail('email',[
                'label' => 'E-Mail Adresse'
            ])
            ->addNumber('phone',[
                'label' => 'Telefon'
            ])
            ->addText('position',[
                'label' => 'Position/Aufgabe des Mitarbeiter',
                'new_lines' => 'wpautop',
            ]);

        return $employee->build();
    }
}
