<?php

$products = 'Produkte';
$product = 'Produkt';
$employee = 'Mitarbeiter';
$employees = 'Mitarbeiter';
$focustopic = 'Fokusthema';
$focustopics = 'Fokusthemen';

return [

    /*
    |--------------------------------------------------------------------------
    | Post Types
    |--------------------------------------------------------------------------
    |
    | Here you may specify the post types to be registered by Poet using the
    | Extended CPTs library. <https://github.com/johnbillion/extended-cpts>
    |
    */

    'post' => [
        'employee' => [
            'enter_title_here' => 'Name hier eintragen',
            'menu_icon' => 'dashicons-businessperson',
            'menu_position' => 21,
            'supports' => ['title','thumbnail'],
            'show_in_rest' => true,
            'has_archive' => false,
            'labels' => [
                'name' => $employee,
                'singular_name' => $employee,
                'menu_name' => $employees,
                'all_items' => sprintf( 'Alle %s', $employees),
                'edit_item' => sprintf( '%s bearbeiten', $employees),
                'view_item' => sprintf( '%s anzeigen', $employee),
                'view_items' => sprintf( '%s anzeigen', $employees),
                'add_new' => 'Neu hinzufügen',
                'add_new_item' => sprintf( 'Neues %s hinzufügen', $employee),
                'new_item' => sprintf( 'Neues %s', $employee),
                'parent_item_colon' => sprintf( 'Übergeordnetes %s', $employee),
                'search_items' => sprintf( '%s suchen', $employees),
                'not_found' =>  sprintf( 'Kein %s gefunden', $employee),
                'not_found_in_trash' => sprintf( 'Kein %s im Papierkorb gefunden', $employee),
                'archives' => sprintf( '%s Archiv', $employees),
                'attributes' => sprintf( '%sattribute', $employee),
                'insert_into_item' => sprintf( 'In %s einfügen', $employee),
                'uploaded_to_this_item' => sprintf( 'Zu %s hochladen', $employee),
                'filter_items_list' => sprintf( '%sliste filtern', $employee),
                'filter_by_date' => sprintf( '%s nach Datum filtern', $employees),
                'items_list_navigation' => sprintf( '%s Listenavigation', $employees),
                'items_list' => sprintf( '%sliste', $employee),
                'item_published' => sprintf( '%s veröffentlicht', $employee),
                'item_published_privately' => sprintf( '%s privat veröffentlicht', $employee),
                'item_reverted_to_draft' => sprintf( '%s als Entwurf zurückgesetzt', $employee),
                'item_scheduled' => sprintf( '%s planen', $employee),
                'item_updated' => sprintf( '%s aktualisiert', $employee),
                'item_link' => sprintf( '%slink', $employee),
                'item_link_description' => sprintf( '%slink Beschreibung', $employee),
            ],
        ],
        'product' => [
            'enter_title_here' => 'Name hier eintragen',
            'menu_icon' => 'dashicons-products',
            'menu_position' => 22,
            'supports' => ['title','thumbnail','editor','excerpt'],
            'show_in_rest' => true,
            'has_archive' => false,
            'template' => array(
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/header-post_image_and_title',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-info-right',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-quote',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/media-text-image-right-landscape',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-faq',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/contact-employee',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/posts-carousel',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
            ),
            'labels' => [
                'name' => $product,
                'singular_name' => $product,
                'menu_name' => $products,
                'all_items' => sprintf( 'Alle %s', $products),
                'edit_item' => sprintf( '%s bearbeiten', $products),
                'view_item' => sprintf( '%s anzeigen', $product),
                'view_items' => sprintf( '%s anzeigen', $products),
                'add_new' => 'Neu hinzufügen',
                'add_new_item' => sprintf( 'Neues %s hinzufügen', $product),
                'new_item' => sprintf( 'Neues %s', $product),
                'parent_item_colon' => sprintf( 'Übergeordnetes %s', $product),
                'search_items' => sprintf( '%s suchen', $products),
                'not_found' =>  sprintf( 'Kein %s gefunden', $product),
                'not_found_in_trash' => sprintf( 'Kein %s im Papierkorb gefunden', $product),
                'archives' => sprintf( '%s Archiv', $products),
                'attributes' => sprintf( '%sattribute', $product),
                'insert_into_item' => sprintf( 'In %s einfügen', $product),
                'uploaded_to_this_item' => sprintf( 'Zu %s hochladen', $product),
                'filter_items_list' => sprintf( '%sliste filtern', $product),
                'filter_by_date' => sprintf( '%s nach Datum filtern', $products),
                'items_list_navigation' => sprintf( '%s Listenavigation', $products),
                'items_list' => sprintf( '%sliste', $product),
                'item_published' => sprintf( '%s veröffentlicht', $product),
                'item_published_privately' => sprintf( '%s privat veröffentlicht', $product),
                'item_reverted_to_draft' => sprintf( '%s als Entwurf zurückgesetzt', $product),
                'item_scheduled' => sprintf( '%s planen', $product),
                'item_updated' => sprintf( '%s aktualisiert', $product),
                'item_link' => sprintf( '%slink', $product),
                'item_link_description' => sprintf( '%slink Beschreibung', $product),
            ],
        ],
        'focustopic' => [
            'enter_title_here' => 'Name hier eintragen',
            'menu_icon' => 'dashicons-search',
            'menu_position' => 22,
            'supports' => ['title','thumbnail','editor','excerpt'],
            'show_in_rest' => true,
            'has_archive' => false,
            'template' => array(
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/header-post_image_and_title',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-info-right',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-quote',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/media-text-image-right-landscape',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/text-faq',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/contact-employee',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),
                array(
                    'core/pattern',
                    array(
                        'slug' => 'vvm/posts-carousel',
                        'theme' => 'vvm',
                        'align' => 'full',
                    ),
                ),

            ),
            'labels' => [
                'name' => $focustopics,
                'singular_name' => $focustopic,
                'menu_name' => $focustopics,
                'all_items' => sprintf( 'Alle %s', $focustopics),
                'edit_item' => sprintf( '%s bearbeiten', $focustopics),
                'view_item' => sprintf( '%s anzeigen', $focustopic),
                'view_items' => sprintf( '%s anzeigen', $focustopics),
                'add_new' => 'Neu hinzufügen',
                'add_new_item' => sprintf( 'Neues %s hinzufügen', $focustopic),
                'new_item' => sprintf( 'Neues %s', $focustopic),
                'parent_item_colon' => sprintf( 'Übergeordnetes %s', $focustopic),
                'search_items' => sprintf( '%s suchen', $focustopics),
                'not_found' =>  sprintf( 'Kein %s gefunden', $focustopic),
                'not_found_in_trash' => sprintf( 'Kein %s im Papierkorb gefunden', $focustopic),
                'archives' => sprintf( '%s Archiv', $focustopics),
                'attributes' => sprintf( '%sattribute', $focustopic),
                'insert_into_item' => sprintf( 'In %s einfügen', $focustopic),
                'uploaded_to_this_item' => sprintf( 'Zu %s hochladen', $focustopic),
                'filter_items_list' => sprintf( '%sliste filtern', $focustopic),
                'filter_by_date' => sprintf( '%s nach Datum filtern', $focustopics),
                'items_list_navigation' => sprintf( '%s Listenavigation', $focustopics),
                'items_list' => sprintf( '%sliste', $focustopic),
                'item_published' => sprintf( '%s veröffentlicht', $focustopic),
                'item_published_privately' => sprintf( '%s privat veröffentlicht', $focustopic),
                'item_reverted_to_draft' => sprintf( '%s als Entwurf zurückgesetzt', $focustopic),
                'item_scheduled' => sprintf( '%s planen', $focustopic),
                'item_updated' => sprintf( '%s aktualisiert', $focustopic),
                'item_link' => sprintf( '%slink', $focustopic),
                'item_link_description' => sprintf( '%slink Beschreibung', $focustopic),
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Taxonomies
    |--------------------------------------------------------------------------
    |
    | Here you may specify the taxonomies to be registered by Poet using the
    | Extended CPTs library. <https://github.com/johnbillion/extended-cpts>
    |
    */

    'taxonomy' => [
        'profession' => [
            'links' => ['employee'],
            'meta_box' => 'simple',
            'labels' => array(
                'name' => 'Tätigkeiten',
                'singular_name' => 'Tätigkeit',
                'menu_name' => 'Tätigkeiten',
                'all_items' => 'Alle Tätigkeiten',
                'edit_item' => 'Tätigkeit bearbeiten',
                'view_item' => 'Tätigkeit anzeigen',
                'update_item' => 'Tätigkeit aktualisieren',
                'add_new_item' => 'Neue Tätigkeit',
                'new_item_name' => 'Neuer Tätigkeitsname',
                'search_items' => 'Tätigkeiten durchsuschen',
                'popular_items' => 'Popular Tätigkeiten',
                'separate_items_with_commas' => 'Tätigkeiten mit Komma trennen',
                'add_or_remove_items' => 'Tätigkeiten hinzufügen oder entfernen',
                'choose_from_most_used' => 'Choose from the most used Tätigkeiten',
                'not_found' => 'Keine Tätigkeiten gefunden',
                'no_terms' => 'Keine Tätigkeiten',
                'items_list_navigation' => 'Tätigkeiten Listennavigation',
                'items_list' => 'Tätigkeitenliste',
                'back_to_items' => 'Turück zu den Tätigkeiten',
                'item_link' => 'Tätigkeit Link',
                'item_link_description' => 'A link to a tätigkeit',
            ),
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Blocks
    |--------------------------------------------------------------------------
    |
    | Here you may specify the block types to be registered by Poet and
    | rendered using Blade.
    |
    | Blocks are registered using the `namespace/label` defined when
    | registering the block with the editor. If no namespace is provided,
    | the current theme text domain will be used instead.
    |
    | Given the block `sage/accordion`, your block view would be located at:
    |   ↪ `views/blocks/accordion.blade.php`
    |
    | Block views have the following variables available:
    |   ↪ $data    – An object containing the block data.
    |   ↪ $content – A string containing the InnerBlocks content.
    |                Returns `null` when empty.
    |
    */

    'block' => [
        // 'sage/accordion',
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Categories
    |--------------------------------------------------------------------------
    |
    | Here you may specify block categories to be registered by Poet for use
    | in the editor.
    |
    */

    'block_category' => [
        'vvm' => [
            'title' => 'Voigt Versicherungs Makler',
            'icon' => 'star-filled',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Patterns
    |--------------------------------------------------------------------------
    |
    | Here you may specify block patterns to be registered by Poet for use
    | in the editor.
    |
    | Patterns are registered using the `namespace/label` defined when
    | registering the block with the editor. If no namespace is provided,
    | the current theme text domain will be used instead.
    |
    | Given the pattern `sage/hero`, your pattern content would be located at:
    |   ↪ `views/block-patterns/hero.blade.php`
    |
    | See: https://developer.wordpress.org/reference/functions/register_block_pattern/
    */

    'block_pattern' => [
        // 'sage/hero' => [
        //     'title' => 'Page Hero',
        //     'description' => 'Draw attention to the main focus of the page, and highlight key CTAs',
        //     'categories' => ['all'],
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Pattern Categories
    |--------------------------------------------------------------------------
    |
    | Here you may specify block pattern categories to be registered by Poet for
    | use in the editor.
    |
    */

    'block_pattern_category' => [
        'all' => [
            'label' => 'All Patterns',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Editor Palette
    |--------------------------------------------------------------------------
    |
    | Here you may specify the color palette registered in the Gutenberg
    | editor.
    |
    | A color palette can be passed as an array or by passing the filename of
    | a JSON file containing the palette.
    |
    | If a color is passed a value directly, the slug will automatically be
    | converted to Title Case and used as the color name.
    |
    | If the palette is explicitly set to `true` – Poet will attempt to
    | register the palette using the default `palette.json` filename generated
    | by <https://github.com/roots/palette-webpack-plugin>
    |
    */

    'palette' => [
        // 'red' => '#ff0000',
        // 'blue' => '#0000ff',
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin Menu
    |--------------------------------------------------------------------------
    |
    | Here you may specify admin menu item page slugs you would like moved to
    | the Tools menu in an attempt to clean up unwanted core/plugin bloat.
    |
    | Alternatively, you may also explicitly pass `false` to any menu item to
    | remove it entirely.
    |
    */

    'admin_menu' => [
        // 'gutenberg',
    ],

];
