@php
  $children = array();
@endphp
<div class="{!! $block->classes !!}" style="{!! $block->inlineStyle !!}">
  @if($menu)
    <nav class="flex flex-row-reverse md:flex-row flex-wrap justify-between items-center pl-2">

        <button id="burger-button" data-toggle-element="mega-menu-full" data-toggle-class="hidden" type="button" class="inline-flex items-center w-8 h-8 py-7 py-1 justify-center text-sm rounded-lg md:hidden text-teal no-underline" aria-controls="mega-menu-full" aria-expanded="false">
          <span class="sr-only">Open main menu</span>
          <svg class="burger" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 32 15" width="32" height="14">
              <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h32 M1 13h32"/>
          </svg>
          @svg('close', '' . ' w-3/4 h-auto icon hidden close fill-teal')
        </button>

        <div id="mega-menu-full" class="absolute h-[calc(100vh-100%)] top-full md:static inset-x-0 items-center justify-between hidden w-full md:flex md:order-1 bg-teal md:bg-transparent py-12 md:py-3 z-40 overflow-y-auto md:overflow-y-visible">
          <ul class="flex flex-col md:flex-row font-medium w-[90%] md:w-auto mx-auto md:mx-0 py-1">
            
            @foreach($menu as $item)
              @php
                $activeClass = ($block->post_id == $item['object_id']) ? 'underline' : '';
              @endphp

              <li class="item w-full md:w-auto">
              
                @if(!empty($item['children']))
                  @php
                  $children[$item['ID']]['children'] = $item['children'];
                  $children[$item['ID']]['parent'] = $item['ID'];
                  @endphp
                  <button id="mega-menu-full-dropdown-button" data-toggle-element="dropdown-{!! $item['ID'] !!}" data-toggle-class="hidden" class="mega-menu-button flex items-center justify-between py-3 md:px-3 text-white md:text-teal font-medium md:w-auto uppercase">
                    <span class="hover:underline underline-offset-3 text-xl md:text-base">{!! $item['title'] !!}</span>
                  </button>
                @else
                  <a href="{!! $item['url'] !!}" class="text-left text-white md:text-teal block uppercase py-3 md:px-3 text-xl md:text-base {!! $activeClass !!}">
                    {!! $item['title'] !!}
                  </a>
                @endif

                @if($item['children'])
                  <div id="dropdown-{!! $item['ID'] !!}" class="dropdown relative md:absolute md:top-full md:h-[calc(100vh-100%)] hidden bg-teal w-full inset-x-0">
                    <div class="mx-auto py-8 md:py-12 md:w-[86%] mx-auto">
                      <ul aria-labelledby="mega-menu-full-dropdown-button" class="md:grid md:grid-cols-3 gap-x-4 text-white">
                        
                        @foreach($item['children'] as $childitem)

                          <li class="item">
                            
                            <div class="mb-10">
                              @if (!empty($childitem['url']))
                                <a href="{!! $childitem['url'] !!}" class="uppercase block py-2 border-white border-b font-bold hover:no-underline text-lg md:text-base">
                                  {!! $childitem['title'] !!}
                                </a>
                              @else
                                <span class="uppercase block py-2 border-white border-b font-bold hover:no-underline text-lg md:text-base">
                                  {!! $childitem['title'] !!}
                                </span>
                              @endif
                              @if(!empty($childitem['description']))
                                <span class="font-bold text-lg md:text-base pr-5 block leading-5 mt-6">{!! $childitem['description'] !!}</span>
                              @endif
                            </div>
        
                            @if(!empty($childitem['children']))
                              <ul class="submenu pr-5">
                                @foreach($childitem['children'] as $childitem)
                                  @php
                                    $activeClass = ($block->post_id == $childitem['object_id']) ? 'underline underline-offset-3' : '';
                                  @endphp

                                  <li class="item mb-4">
                                    <a href="{!! $childitem['url'] !!}" class="uppercase text-lg md:text-base hyphens-auto {!! $activeClass !!}">
                                      {!! $childitem['title'] !!}
                                    </a>
                                    @if(!empty($childitem['description']))
                                      <span class="link-desc block text-white/50 normal-case text-lg md:text-base leading-5">{!! $childitem['description'] !!}</span>
                                    @endif
                                  </li>
                                @endforeach
                              </ul>
                            @endif

                          </li>
                        @endforeach

                      </ul>
                    </div>
                  </div>
              @endif

              </li>
            @endforeach

          </ul>
        </div>

    </nav>
  @endif
</div>
