<div id="{{ $block->block->anchor ?? '' }}" class="{{ $block->classes }}">
  @hasposts($query)
  <div class="grid sm:grid-cols-2 md:grid-cols-3 gap-4">
    @posts($query)

      @php
        $id = get_the_ID();
      @endphp

      <div class="employee flex flex-col text-white">
        @if (get_the_post_thumbnail())
          <div class="image-wrapper aspect-square overflow-hidden">
            @php
              the_post_thumbnail(
                'portrait',
                ['class' => 'w-full object-cover']
              );
            @endphp
          </div>
        @endif

        <div class="relative p-7 pt-28 bg-gradient-to-t from-teal from-65% flex flex-col justify-between grow -mt-28">
          <div class="mb-5">
            <h5 class="font-sans font-black">
              @if(get_field('title', $id))
                {!! get_field('title', $id) . ' ' !!}
              @endif
              @if(get_field('first-name', $id))
                {!! get_field('first-name', $id) . ' ' !!}
              @endif
              @if(get_field('last-name', $id))
                {!! get_field('last-name', $id) !!}
              @endif
              @if(get_field('abbr', $id))
                {!! ' (' . get_field('abbr', $id) . ')' !!}
              @endif
            </h5>
            @if(get_field('position', $id))
              <div>{!! get_field('position', $id) !!}</div>
            @endif
          </div>
          <div class="flex">
            @if(get_field('email', $id))
              <a href="mailto:{!! get_field('email', $id) !!}" target="_blank" class="w-10 h-10 bg-white rounded-full flex items-center me-2 hover:bg-yellow">
                @svg('email-action-unread','stroke-teal w-24 icon')
              </a>
            @endif
            @if(get_field('phone', $id))
            <a href="tel:{{ get_field('phone', $id) }}" class="w-10 h-10 bg-white rounded-full flex items-center hover:bg-yellow" target="_blank">
                @svg('phone','stroke-teal w-24 icon')
              </a>
            @endif
          </div>
        </div>

      </div>

    @endposts
  </div>
  @endhasposts
</div>
