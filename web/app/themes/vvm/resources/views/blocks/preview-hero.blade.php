<div id="{{ $block->block->anchor ?? '' }}" class="{{ $block->classes }}">
  <div class="grid lg:grid-cols-2 hero">
    <div class="relative hero-left">
      <div class="hero-image">
        @if($item['hero_image'])
          @image($item['hero_image'], 'large_square', false)
        @else
          <div class="placeholder-image">
          </div>
        @endif
      </div>
      <div class="bg-yellow text-teal font-handwritten is-layout-constrained relative slogan"  style="padding-top:var(--wp--preset--spacing--40); padding-bottom:var(--wp--preset--spacing--40)">
        <div>
          <img src="@asset('images/vertrauen_versichern_machen.svg')" style="max-width: 600px; width: 100%; margin: auto;"/>
        </div>
      </div>
    </div>
    <div class="relative bg-white lg:max-w-4xl lg:px-10 is-layout-constrained">
      <div class="h-full w-full">
        <InnerBlocks
          template="{!! $template !!}"
          allowedBlocks="{!! $allowed_blocks !!}"
        />
      </div>
    </div>
  </div>
</div>