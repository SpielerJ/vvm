<div class="fixed bottom-0 w-full z-10">
  <div id="{{ $block->block->anchor ?? '' }}" class="absolute bottom-0 w-full {{ $block->classes }}" style="">
    <button id="contact-button" class="absolute right-0 bottom-0 aspect-square rounded-full bg-yellow flex items-center justify-center z-10 shadow-xl fill-teal hover:fill-yellow hover:bg-teal" style="width: 60px;" data-toggle-element="contact-slideout" data-toggle-class="hidden">
      @svg('talk_VVM', '' . ' w-3/5 h-auto icon talk')
      @svg('close', '' . ' w-2/5 h-auto icon hidden close')
    </button>
    @hasposts($query)
    <div id="contact-slideout" class="absolute max-w-full right-0 bottom-0 bg-white border border-gray/30 px-4 py-6 z-0 shadow-xl hidden flex flex-col items-start">
        <h4 class="font-display text-teal uppercase mb-2">Expert:innen</h4>

        <ul class="employee grid gap-1 text-teal mb-6 w-full">
          @posts($query)
          
            @php
              $roundedClass = '';
              if ($query->current_post == 0) {
                $roundedClass .= 'rounded-t-lg';
              }
              if ($query->current_post +1 == $query->post_count) {
                $roundedClass .= ' rounded-b-lg';
              }
              $id = get_the_ID();
            @endphp
            
            <li class="relative p-3 bg-gray/10 border border-gray/20 flex justify-between items-center {{ $roundedClass }}">
              @if (get_the_post_thumbnail())
                @php
                  the_post_thumbnail(
                    'thumbnail',
                    ['class' => 'w-16 object-cover rounded-full border border-gray/50']
                  );
                @endphp
              @endif
              <div class="grow ms-3">
                <h6 class="font-sans font-black !text-sm">
                  @if(get_field('title', $id))
                    {!! get_field('title', $id) . ' ' !!}
                  @endif
                  @if(get_field('first-name', $id))
                    {!! get_field('first-name', $id) . ' ' !!}
                  @endif
                  @if(get_field('last-name', $id))
                    {!! get_field('last-name', $id) !!}
                  @endif
                  @if(get_field('abbr', $id))
                    {!! ' (' . get_field('abbr', $id) . ')' !!}
                  @endif
                </h6>
                @if(get_field('position', $id))
                  <div class="text-sm hyphens-auto">{!! get_field('position', $id) !!}</div>
                @endif
              </div>
              <div class="flex">
                @if(get_field('phone', $id))
                  <a href="tel:{{ get_field('phone', $id) }}" class="stroke-teal hover:stroke-yellow w-8 h-8 bg-white hover:bg-teal rounded-full flex items-center justify-center ms-2" target="_blank">
                    @svg('phone','w-3/5 icon')
                  </a>
                @endif
                @if(get_field('email', $id))
                  <a href="mailto:{!! get_field('email', $id) !!}" target="_blank" class="stroke-teal hover:stroke-yellow w-8 h-8 bg-white hover:bg-teal rounded-full flex items-center justify-center ms-2">
                    @svg('email-action-unread','w-3/5 icon')
                  </a>
                @endif
              </div>
            </li>
          
          @endposts
        </ul>

        @if (isset($item['link']))
          <div class="wp-block-button">
            <a href="{{ $item['link']['url'] }}" target="{{ $item['link']['target'] }}" class="wp-block-button__link wp-element-button bg-yellow text-teal hover:text-yellow hover:bg-teal fw-bold">{{ $item['link']['title'] }}</a>
          </div>
        @endif

    </div>
    @endhasposts
  </div>
</div>
