
@php
  $color = (isset($block->block->textColor)) ? $block->block->textColor : 'teal';
@endphp

<div id="{{ $block->block->anchor ?? '' }}" class="{{ $block->classes }}">
  @hasposts($query)
  <div class="wp-block-columns is-layout-flex wp-block-columns-is-layout-flex" style="">
    @posts($query)

      @php
        $id = get_the_ID();
      @endphp

      <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
        @if (get_the_post_thumbnail())
          <div class="image-wrapper overflow-hidden max-md:!pr-0" style="padding-right: var(--wp--preset--spacing--70);">
            @php
              the_post_thumbnail(
                'portrait',
                ['class' => 'w-full object-cover']
              );
            @endphp
          </div>
        @endif
      </div>

      <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow has-{{ $color }}-color has-text-color">
        <h3 class="wp-block-heading has-6-xl-font-size" style="margin-bottom: var(--wp--preset--spacing--60);">{!! $heading !!}</h3>
        <div class="mb-8">
          <h4 class="font-sans font-black">
            @if(get_field('title', $id))
              {!! get_field('title', $id) . ' ' !!}
            @endif
            @if(get_field('first-name', $id))
              {!! get_field('first-name', $id) . ' ' !!}
            @endif
            @if(get_field('last-name', $id))
              {!! get_field('last-name', $id) !!}
            @endif
            @if(get_field('abbr', $id))
              {!! ' (' . get_field('abbr', $id) . ')' !!}
            @endif
          </h4>
          @if(get_field('position', $id))
            <div>{!! get_field('position', $id) !!}</div>
          @endif
        </div>
        <div class="wp-block-buttons is-vertical is-layout-flex wp-block-buttons-is-layout-flex is-content-justification-left">
          @if(get_field('phone', $id))
            <div class="wp-block-button">
              <a href="tel:{{ get_field('phone', $id) }}" class="wp-block-button__link has-teal-color has-yellow-background-color has-text-color has-background wp-element-button" target="_blank">
                Telefon
              </a>
            </div>
          @endif
          @if(get_field('email', $id))
            <div class="wp-block-button">
              <a href="mailto:{!! get_field('email', $id) !!}" target="_blank" class="wp-block-button__link has-teal-color has-yellow-background-color has-text-color has-background wp-element-button">
                E-Mail
              </a>
            </div>
          @endif
        </div>
      </div>

    @endposts
  </div>
  @endhasposts
</div>
