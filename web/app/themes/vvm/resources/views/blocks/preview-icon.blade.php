<div class="{{ $block->classes }}">
  @if ($item['icon'])
      @php
        if ($item['auto-width'] == true) {
          $style = 'width: 100%; max-width: 206px; margin-left: auto; margin-right: auto;';
        }
        else {
          $style = 'width: ' . $item['width'] . 'px;';
        }
      @endphp
    <figure class="block" style="{{$style}}">
        @if ($item['link'])
          <a href="{{ $item['link']['url'] }}" title="{{ $item['link']['title'] }}" target="{{ $item['link']['target'] }}">
        @endif
          @if ($item['background_color'])
            <div class="icon-container aspect-square rounded-full has-{{ $item['background_color'] }}-background-color flex items-center justify-center">
              @svg($item['icon'], 'stroke-' . $item['icon_color'] . ' w-2/5 h-auto icon')
            </div>
          @else
            <div class="icon-container">
              @svg($item['icon'], 'stroke-' . $item['icon_color'] . ' w-full h-auto icon')
            </div>
          @endif
        @if ($item['link'])
          </a>
        @endif

        @if ($item['caption'])
          <figcaption class="text-center uppercase has-{{ $item['text_color'] }}-color font-bold mt-4 hyphens-auto">
            @if ($item['link'])
              <a href="{{ $item['link']['url'] }}" title="{{ $item['link']['title'] }}" target="{{ $item['link']['target'] }}" class="text-decoration-none">
            @endif
              {{ $item['caption'] }}
            @if ($item['link'])
              </a>
            @endif
          </figcaption>
        @endif
    </figure>

  @else
    <p>{{ $block->preview ? 'Add an item...' : 'No items found!' }}</p>
  @endif
</div>


