<div id="{{ $block->block->anchor ?? '' }}" class="{{ $block->classes }} grid grid-cols-12 gap-4" style="{{ $block->inlineStyle }}">
   @hasposts($query)
      @if($query->post_count > 1)
        <div class="post-carousel owl-carousel col-span-12 md:col-start-2 md:col-span-10 xl:col-start-3 xl:col-span-8">
      @else
        <div class="col-span-12 md:col-start-2 md:col-span-10 xl:col-start-3 xl:col-span-8">
      @endif
      @posts
          <div class="lg:aspect-[3/2] bg-teal h-full relative">
            <a class="block absolute h-full w-full" href="{{ get_permalink() }}" title="@title">

              @if(has_post_thumbnail())
                @php
                  the_post_thumbnail(
                    'small-landscape',
                    ['class' => 'object-cover w-full h-full']
                  );
                @endphp
                <div class="absolute inset-0 bg-black/30"></div>
              @endif

            </a>
            <div class="relative inset-0 flex flex-col text-yellow h-full justify-between items-start sm:w-3/4 lg:w-1/2 max-md:!px-6 p-12" style="">
              <div class="mb-28 lg:grow lg:mb-0">
                <h4 class="font-display has-6-xl-font-size leading-none uppercase" style="margin-bottom: var(--wp--preset--spacing--30);">
                  <a href="{{ get_permalink() }}" class="text-decoration-none" title="@title">
                    @title
                  </a>
                </h4>
                <div class="text-xl w-3/4 sm:w-full">
                  @excerpt
                </div>
              </div>
              <div class="wp-block-button">
                <a class="wp-block-button__link has-teal-color has-yellow-background-color has-text-color has-background wp-element-button" href="{!! get_permalink() !!}" title="{{ get_the_title() }}">{{ __('Continued', 'sage') }}</a>
              </div>
            </div>
          </div>
      @endposts
    </div>
  @endhasposts
</div>