import {assign} from 'lodash'

export default {
/* Filter name */
  name: 'sage/list/example',

/* Hook id */
  hook: 'blocks.registerBlockType',

  callback: (settings, name) => {
    if (name !== 'core/list') return settings;
  
    return assign({}, settings, {
        supports: {
            className: true,
        }
      }
    )
  }
}

