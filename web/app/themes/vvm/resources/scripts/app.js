import domReady from '@roots/sage/client/dom-ready';

// Import OWL Carousel
import 'owl.carousel';

/**
 * Application entrypoint
 */
domReady(async () => {
  
  $('.post-carousel').each(function(){
    $(this).owlCarousel({
      items: 1,
      loop: true,
      margin: 16,
      nav: false,
      dots: true,
    });
  });

  if(window.sessionStorage.getItem('visit') != 'true' && $('.intro').length > 0) {
    $('.intro').css('display', 'flex');
    window.sessionStorage.setItem('visit', 'true');
  }

  $(document).on('click', 'button[data-toggle-element]:not(.mega-menu-button):not(#burger-button):not(#contact-button)', function(){
    var toggleClass = $(this).data('toggle-class');
    var toggleElement = $(this).data('toggle-element');
    $('#' + toggleElement).toggleClass(toggleClass);
    $(this).toggleClass('active');
  });
  

  var contactIsOpen = false;
  
  $(document).on('click', '#contact-button', function(e){
    var toggleClass = $(this).data('toggle-class');
    var toggleElement = $(this).data('toggle-element');
    $('#' + toggleElement).toggleClass(toggleClass);
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      contactIsOpen = true;
    }
    else {
      contactIsOpen = false;
    }
  });

  var megamenuIsOpen = false;
  var burgerMenuIsOpen = false;

  $(document).on('click', '.mega-menu-button', function(e){
    var toggleClass = $(this).data('toggle-class');
    var toggleElement = $(this).data('toggle-element');
    $('#' + toggleElement).toggleClass(toggleClass);
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      megamenuIsOpen = true;
    }
    else {
      megamenuIsOpen = false;
    }
    if (megamenuIsOpen == true && burgerMenuIsOpen != true) {
      $('body').addClass('no-scroll');
    }
    else if (burgerMenuIsOpen != true) {
      $('body').removeClass('no-scroll');
    }
  });


  $('#burger-button').on('click', function(e) {
    megamenuIsOpen = false;
    var toggleClass = $(this).data('toggle-class');
    var toggleElement = $(this).data('toggle-element');
    $('#' + toggleElement).toggleClass(toggleClass);
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      burgerMenuIsOpen = true;
    }
    else {
      burgerMenuIsOpen = false;
    }
    if (burgerMenuIsOpen == true ) {
      $('body').addClass('no-scroll');
    }
    else {
      $('body').removeClass('no-scroll');
    }
  })


  $(document).on('click', function(e){
    if (
      !$(e.target).is('.dropdown') &&
      !$('.dropdown').find(e.target).length &&
      !$(e.target).is('.mega-menu-button') &&
      !$('.mega-menu-button').find(e.target).length &&
      megamenuIsOpen == true
    ) {
      if (burgerMenuIsOpen == false) {
        $('body').removeClass('no-scroll');
      }
      $('.dropdown').addClass('hidden');
      megamenuIsOpen == false;
      $('.mega-menu-button').removeClass('active');
    }

    if (
      !$(e.target).is('#contact-slideout') &&
      !$('#contact-slideout').find(e.target).length &&
      !$(e.target).is('#contact-button') &&
      !$('#contact-button').find(e.target).length &&
      contactIsOpen == true
    ) {
      $('#contact-slideout').addClass('hidden');
      $('#contact-button').removeClass('active');
    }
  });

});


/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
