/**
 * @see {@link https://bud.js.org/extensions/bud-preset-wordpress/editor-integration/filters}
 */

// Import OWL Carousel
import 'owl.carousel';

/**
 * @see {@link https://bud.js.org/extensions/bud-preset-wordpress/editor-integration/filters}
 */
roots.register.filters('@scripts/filters');

wp.domReady( () => {
  wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );

  var initPostCarousel = function( block ) {
    block.find('.post-carousel').owlCarousel({
      items: 1,
      loop: true,
      margin: 16,
      nav: false,
      dots: true,
    });
  }

  if( window.acf ) {
    window.acf.addAction( 'render_block_preview/type=postcarousel', initPostCarousel );
  }

} );

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
