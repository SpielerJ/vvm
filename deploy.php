<?php

namespace Deployer;

require_once(__DIR__ . '/vendor/sourcebroker/deployer-loader/autoload.php');
require 'sage.php';
new \SourceBroker\DeployerExtendedWordpressComposer\Loader();

set('repository', 'https://gitlab.com/SpielerJ/vvm.git');
set('keep_releases', 3);
task('db:backup')->disable();
task('db:truncate')->disable();
task('deploy:clear_paths')->disable();
task('cache:clear_php_cli')->disable();
task('assets:build')->disable();
after('deploy:vendors', 'sage:vendors');
after('sage:vendors', 'push:assets');
set('bin/php', function () {
    return 'php';
});
task('deploy:vendors', function () {
    if (!commandExist('unzip')) {
        warning('To speed up composer installation setup "unzip" command with PHP zip extension.');
    }
    run('cd {{release_or_current_path}} && {{bin/composer}} {{composer_action}} --working-dir={{release_or_current_path}} {{composer_options}} 2>&1');
});

host('staging')
    ->set('ssh_multiplexing', true)
    ->setHostname('ssh.strato.de')
    ->setRemoteUser('meta-maniacs.de')
    ->set('branch', 'main')
    ->set('public_urls', ['http://vvm.meta-maniacs.de'])
    ->set('http_user', 'customer')
    ->set('shared_files', array_merge(get('shared_files'), ['.env']))
    ->set('writable_mode', 'chmod')
    ->set('deploy_path', '/mnt/rid/44/85/59764485/htdocs/vvm')
    ->set( 'local_root', dirname( __FILE__ ) )
    ->set( 'theme_path', 'web/app/themes/vvm' )
    ->set( 'bin/composer', '~/vvm/.dep/composer.phar' );

host('local')
    ->set('public_urls', ['https://vvm.ddev.site'])
    ->set('deploy_path', getcwd())
    ->set( 'local_root', dirname( __FILE__ ) )
    ->set( 'theme_path', 'web/app/themes/vvm' )
    ->set('db_databases', [
      'database_default' => [
          (new \SourceBroker\DeployerExtendedDatabase\Driver\EnvDriver())->getDatabaseConfig()
      ]
    ]);